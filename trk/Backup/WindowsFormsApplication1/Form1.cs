﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] str = System.IO.Ports.SerialPort.GetPortNames();
            //this.listBox1.Items.AddRange(new object[] {"456"});
            listBox1.Items.AddRange(str);
            listBox1.SetSelected(0, true);
            
        } 
        
        private void button1_Click(object sender, EventArgs e)
        {
            sens_request();
            //sens_request_2();
            //sens_request_3();
            //sens_request_4();
            //sens_request_5();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            button2.Enabled = false;
            button3.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            sens_request();
            //sens_request_2();
            //sens_request_3();
            //sens_request_4();
            //sens_request_5();
        }

        

        private void button3_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            button2.Enabled = true;
            button3.Enabled = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            serialPort1.PortName = listBox1.SelectedItem.ToString();
            serialPort1.Open();
        }

        private void button5_Click(object sender, EventArgs e1)
        {
            byte byte13;
            byte byte14;
            byte byte110;
            long a;
            a = long.Parse(textBox1.Text);
            textBox2.Text = a.ToString();
            byte13 = (byte)((short)a >> 8);
            byte14 = (byte)a;
            byte110 = (byte)(byte13 + byte14 + 0x42);
            byte[] array = { 0x01, 0x41, 0x00, byte13, byte14, 0x00, 0x00, 0x00, 0x00, 0x00, byte110 };    // каллибровка первого порога
            serialPort1.Write(array, 0, 11);
            Int32 byte0;
            Int32 byte1;
            Int32 byte2;
            Int32 byte3;
            Int32 byte4;
            Int32 byte5;
            Int32 byte6;
            Int32 byte7;
            Int32 byte8;
            Int32 byte9;
            Int32 byte10;
            //Int32 ex_to = 0;
            try
            {
                 byte0 = serialPort1.ReadByte();
                 byte1 = serialPort1.ReadByte();
                 byte2 = serialPort1.ReadByte();
                 byte3 = serialPort1.ReadByte();
                 byte4 = serialPort1.ReadByte();
                 byte5 = serialPort1.ReadByte();
                 byte6 = serialPort1.ReadByte();
                 byte7 = serialPort1.ReadByte();
                 byte8 = serialPort1.ReadByte();
                 byte9 = serialPort1.ReadByte();
                 byte10 = serialPort1.ReadByte();
            }
            catch (TimeoutException)
            {
                //return;
                //Console.WriteLine(e);
                byte0 = 0; byte1 = 0; byte2 = 0; byte3 = 0; byte4 = 0; byte5 = 0; byte6 = 0; byte7 = 0; byte8 = 0; byte9 = 0; byte10 = 0;
                //ex_to = 1;
            }



        }

        private void button14_Click(object sender, EventArgs e1)
        {
            byte byte13;
            byte byte14;
            byte byte110;
            long a;
            a = long.Parse(textBox11.Text);
            textBox15.Text = a.ToString();
            byte13 = (byte)((short)a >> 8);
            byte14 = (byte)a;
            byte110 = (byte)(byte13 + byte14 + 0x43);
            byte[] array = { 0x01, 0x42, 0x00, byte13, byte14, 0x00, 0x00, 0x00, 0x00, 0x00, byte110 };    // каллибровка второго порога
            serialPort1.Write(array, 0, 11);
            Int32 byte0;
            Int32 byte1;
            Int32 byte2;
            Int32 byte3;
            Int32 byte4;
            Int32 byte5;
            Int32 byte6;
            Int32 byte7;
            Int32 byte8;
            Int32 byte9;
            Int32 byte10;
            //Int32 ex_to = 0;
            try
            {
                byte0 = serialPort1.ReadByte();
                byte1 = serialPort1.ReadByte();
                byte2 = serialPort1.ReadByte();
                byte3 = serialPort1.ReadByte();
                byte4 = serialPort1.ReadByte();
                byte5 = serialPort1.ReadByte();
                byte6 = serialPort1.ReadByte();
                byte7 = serialPort1.ReadByte();
                byte8 = serialPort1.ReadByte();
                byte9 = serialPort1.ReadByte();
                byte10 = serialPort1.ReadByte();
            }
            catch (TimeoutException)
            {
                //return;
                //Console.WriteLine(e);
                byte0 = 0; byte1 = 0; byte2 = 0; byte3 = 0; byte4 = 0; byte5 = 0; byte6 = 0; byte7 = 0; byte8 = 0; byte9 = 0; byte10 = 0;
                //ex_to = 1;
            }



        }
        private void button15_Click(object sender, EventArgs e1)
        {
            byte byte13;
            byte byte14;
            byte byte110;
            long a;
            a = long.Parse(textBox12.Text);
            textBox16.Text = a.ToString();
            byte13 = (byte)((short)a >> 8);
            byte14 = (byte)a;
            byte110 = (byte)(byte13 + byte14 + 0x44);
            byte[] array = { 0x01, 0x43, 0x00, byte13, byte14, 0x00, 0x00, 0x00, 0x00, 0x00, byte110 };    // каллибровка третьего порога
            serialPort1.Write(array, 0, 11);
            Int32 byte0;
            Int32 byte1;
            Int32 byte2;
            Int32 byte3;
            Int32 byte4;
            Int32 byte5;
            Int32 byte6;
            Int32 byte7;
            Int32 byte8;
            Int32 byte9;
            Int32 byte10;
            //Int32 ex_to = 0;
            try
            {
                byte0 = serialPort1.ReadByte();
                byte1 = serialPort1.ReadByte();
                byte2 = serialPort1.ReadByte();
                byte3 = serialPort1.ReadByte();
                byte4 = serialPort1.ReadByte();
                byte5 = serialPort1.ReadByte();
                byte6 = serialPort1.ReadByte();
                byte7 = serialPort1.ReadByte();
                byte8 = serialPort1.ReadByte();
                byte9 = serialPort1.ReadByte();
                byte10 = serialPort1.ReadByte();
            }
            catch (TimeoutException)
            {
                //return;
                //Console.WriteLine(e);
                byte0 = 0; byte1 = 0; byte2 = 0; byte3 = 0; byte4 = 0; byte5 = 0; byte6 = 0; byte7 = 0; byte8 = 0; byte9 = 0; byte10 = 0;
                //ex_to = 1;
            }
        


        }


        private void button16_Click(object sender, EventArgs e1)
        {
            byte byte13;
            byte byte14;
            byte byte110;
            long a;
            a = long.Parse(textBox13.Text);
            textBox17.Text = a.ToString();
            byte13 = (byte)((short)a >> 8);
            byte14 = (byte)a;
            byte110 = (byte)(byte13 + byte14 + 0x46);
            byte[] array = { 0x01, 0x45, 0x00, byte13, byte14, 0x00, 0x00, 0x00, 0x00, 0x00, byte110 };    // каллибровка четвёртого порога
            serialPort1.Write(array, 0, 11);
            Int32 byte0;
            Int32 byte1;
            Int32 byte2;
            Int32 byte3;
            Int32 byte4;
            Int32 byte5;
            Int32 byte6;
            Int32 byte7;
            Int32 byte8;
            Int32 byte9;
            Int32 byte10;
            //Int32 ex_to = 0;
            try
            {
                byte0 = serialPort1.ReadByte();
                byte1 = serialPort1.ReadByte();
                byte2 = serialPort1.ReadByte();
                byte3 = serialPort1.ReadByte();
                byte4 = serialPort1.ReadByte();
                byte5 = serialPort1.ReadByte();
                byte6 = serialPort1.ReadByte();
                byte7 = serialPort1.ReadByte();
                byte8 = serialPort1.ReadByte();
                byte9 = serialPort1.ReadByte();
                byte10 = serialPort1.ReadByte();
            }
            catch (TimeoutException)
            {
                //return;
                //Console.WriteLine(e);
                byte0 = 0; byte1 = 0; byte2 = 0; byte3 = 0; byte4 = 0; byte5 = 0; byte6 = 0; byte7 = 0; byte8 = 0; byte9 = 0; byte10 = 0;
                //ex_to = 1;
            }



        }

        private void button17_Click(object sender, EventArgs e1)
        {
            byte byte13;
            byte byte14;
            byte byte110;
            long a;
            a = long.Parse(textBox14.Text);
            textBox18.Text = a.ToString();
            byte13 = (byte)((short)a >> 8);
            byte14 = (byte)a;
            byte110 = (byte)(byte13 + byte14 + 0x47);
            byte[] array = { 0x01, 0x46, 0x00, byte13, byte14, 0x00, 0x00, 0x00, 0x00, 0x00, byte110 };    // каллибровка пятого порога
            serialPort1.Write(array, 0, 11);
            Int32 byte0;
            Int32 byte1;
            Int32 byte2;
            Int32 byte3;
            Int32 byte4;
            Int32 byte5;
            Int32 byte6;
            Int32 byte7;
            Int32 byte8;
            Int32 byte9;
            Int32 byte10;
            //Int32 ex_to = 0;
            try
            {
                byte0 = serialPort1.ReadByte();
                byte1 = serialPort1.ReadByte();
                byte2 = serialPort1.ReadByte();
                byte3 = serialPort1.ReadByte();
                byte4 = serialPort1.ReadByte();
                byte5 = serialPort1.ReadByte();
                byte6 = serialPort1.ReadByte();
                byte7 = serialPort1.ReadByte();
                byte8 = serialPort1.ReadByte();
                byte9 = serialPort1.ReadByte();
                byte10 = serialPort1.ReadByte();
            }
            catch (TimeoutException)
            {
                //return;
                //Console.WriteLine(e);
                byte0 = 0; byte1 = 0; byte2 = 0; byte3 = 0; byte4 = 0; byte5 = 0; byte6 = 0; byte7 = 0; byte8 = 0; byte9 = 0; byte10 = 0;
                //ex_to = 1;
            }



        }

        private void button18_Click(object sender, EventArgs e1)
        {

            byte[] array = { 0x01, 0x48, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x49 };    // запрос порогов
            serialPort1.Write(array, 0, 11);
            Int32 byte0;
            Int32 byte1;
            Int32 byte2;
            Int32 byte3;
            Int32 byte4;
            Int32 byte5;
            Int32 byte6;
            Int32 byte7;
            Int32 byte8;
            Int32 byte9;
            Int32 byte10;
            Int32 byte11;
            Int32 byte12;
            Int32 byte13;
            Int32 byte14;
            Int32 byte15;
            Int32 byte16;
            Int32 byte17;
            Int32 byte18;
            Int32 byte19;
            Int32 byte20;
            Int32 byte21;
            Int32 byte22;
            //Int32 ex_to = 0;
            try
            {
                byte0 = serialPort1.ReadByte();
                byte1 = serialPort1.ReadByte();
                byte2 = serialPort1.ReadByte();
                byte3 = serialPort1.ReadByte();
                byte4 = serialPort1.ReadByte();
                byte5 = serialPort1.ReadByte();
                byte6 = serialPort1.ReadByte();
                byte7 = serialPort1.ReadByte();
                byte8 = serialPort1.ReadByte();
                byte9 = serialPort1.ReadByte();
                byte10 = serialPort1.ReadByte();
                byte11 = serialPort1.ReadByte();
                byte12 = serialPort1.ReadByte();
                byte13 = serialPort1.ReadByte();
                byte14 = serialPort1.ReadByte();
                byte15 = serialPort1.ReadByte();
                byte16 = serialPort1.ReadByte();
                byte17 = serialPort1.ReadByte();
                byte18 = serialPort1.ReadByte();
                byte19 = serialPort1.ReadByte();
                byte20 = serialPort1.ReadByte();
                byte21 = serialPort1.ReadByte();
                byte22 = serialPort1.ReadByte();
            }
            catch (TimeoutException)
            {
                //return;
                //Console.WriteLine(e);
                byte0 = 0; byte1 = 0; byte2 = 0; byte3 = 0; byte4 = 0; byte5 = 0; byte6 = 0; byte7 = 0; byte8 = 0; byte9 = 0; byte10 = 0;
                byte11 = 0; byte12 = 0; byte13 = 0; byte14 = 0; byte15 = 0; byte16 = 0; byte17 = 0; byte18 = 0; byte19 = 0; byte20 = 0; byte21 = 0; byte22 = 0;
                //ex_to = 1;
            }
            Int32 adc_porog_1 = byte3;
            adc_porog_1 = adc_porog_1 | (byte2 << 8);
            textBox1.Text = adc_porog_1.ToString();
            Int32 porog_1 = byte13;
            porog_1 = porog_1 | (byte12 << 8);
            textBox2.Text = porog_1.ToString();

            Int32 adc_porog_2 = byte5;
            adc_porog_2 = adc_porog_2 | (byte4 << 8);
            textBox11.Text = adc_porog_2.ToString();
            Int32 porog_2 = byte15;
            porog_2 = porog_2 | (byte14 << 8);
            textBox15.Text = porog_2.ToString();

            Int32 adc_porog_3 = byte7;
            adc_porog_3 = adc_porog_3 | (byte6 << 8);
            textBox12.Text = adc_porog_3.ToString();
            Int32 porog_3 = byte17;
            porog_3 = porog_3 | (byte16 << 8);
            textBox16.Text = porog_3.ToString();

            Int32 adc_porog_4 = byte9;
            adc_porog_4 = adc_porog_4 | (byte8 << 8);
            textBox13.Text = adc_porog_4.ToString();
            Int32 porog_4 = byte19;
            porog_4 = porog_4 | (byte18 << 8);
            textBox17.Text = porog_4.ToString();

            Int32 adc_porog_5 = byte11;
            adc_porog_5 = adc_porog_5 | (byte10 << 8);
            textBox14.Text = adc_porog_5.ToString();
            Int32 porog_5 = byte21;
            porog_5 = porog_5 | (byte20 << 8);
            textBox18.Text = porog_5.ToString();
        }

        private void sens_request()
        {


            byte[] array = { 0x01, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x45 };    // запрос концентрации
            serialPort1.Write(array, 0, 11);
            
            Int32 byte0;
            Int32 byte1;
            Int32 byte2;
            Int32 byte3;
            Int32 byte4;
            Int32 byte5;
            Int32 byte6;
            Int32 byte7;
            Int32 byte8;
            Int32 byte9;
            Int32 byte10;
            Int32 ex_to = 0;

            Int32 result_release;
            short result_release_out;
            double result_release_out_out;
            double temperature;
            Int32 porog;
            Int32 u_adc;

            try
            {
                byte0 = serialPort1.ReadByte();
                byte1 = serialPort1.ReadByte();
                byte2 = serialPort1.ReadByte();
                byte3 = serialPort1.ReadByte();
                byte4 = serialPort1.ReadByte();
                byte5 = serialPort1.ReadByte();
                byte6 = serialPort1.ReadByte();
                byte7 = serialPort1.ReadByte();
                byte8 = serialPort1.ReadByte();
                byte9 = serialPort1.ReadByte();
                byte10 = serialPort1.ReadByte();
            }
            catch (TimeoutException)
            {
                //return;
                //Console.WriteLine(e);
                byte0 = 0; byte1 = 0; byte2 = 0; byte3 = 0; byte4 = 0; byte5 = 0; byte6 = 0; byte7 = 0; byte8 = 0; byte9 = 0; byte10 = 0;
                ex_to = 1;
            }



            /*
            result_release = (double)(((byte6 & 0x80) << 9) | (byte2 << 8) | byte3) / 10;
            porog = byte6 & 0x03;
            temperature = (double)((byte4 << 8) | byte5) / 10;
            */
            if (ex_to == 0)
            {
                result_release = (byte7 << 8);
                result_release = (result_release + byte8);
                result_release_out = (short)result_release;
                result_release_out_out = (double)((double)result_release_out / 1000);

                porog = byte9 & 0x03;
                temperature = (double)((byte4 << 8) | byte5) / 10;

                u_adc = ((byte2 << 8) | byte3);

                label1.Text = result_release_out_out.ToString("f3");

                label8.Text = u_adc.ToString("f0");



                label9.Text = temperature.ToString("f");



                StreamWriter sw;
                sw = File.AppendText("logfile1.txt");
                sw.WriteLine(label1.Text + "  " + label8.Text + "  " + label9.Text + "  " + DateTime.Now);
                sw.Close();
            }
        }

        

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void groupBox8_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox11_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox14_Enter(object sender, EventArgs e)
        {

        }

        /*private void button18_Click(object sender, EventArgs e)
        {

        }
        */
    }
}
