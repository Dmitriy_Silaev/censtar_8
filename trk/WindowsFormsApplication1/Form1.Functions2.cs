﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {


        public void ReadFunct(ref byte CountBufferDebugIn, ref byte[] InBuffer, ref byte chanel, ref uint state, ref uint nozzle)
        {
            byte started_DebugRxd=0;
            byte symvol;
            byte state_DebugRxd=0xFF;
            byte debug_count_crc = 0;
            byte debug_count_crc_stop = 0;
            byte MY_DEBUG_MODE = 0;

            try
            {
                for (byte i = 0; i < 255; i++)
                {
                    InBuffer[i] = (byte)serialPort1.ReadByte();
                    symvol = InBuffer[i];
                    CountBufferDebugIn = i;
                    serialPort1.ReadTimeout = 10;//первый байт принят уменьшаем таймаут до минимума

                    if (CountBufferDebugIn == 0)
                    {
                        if (symvol == 0x02)
                            started_DebugRxd = 1;
                    }
                    if (started_DebugRxd>0)
                    {
                        if (CountBufferDebugIn == 1)
                        {
                            debug_count_crc = symvol;
                        }

                        if (CountBufferDebugIn > 1)
                        {
                            if (debug_count_crc_stop == 0)
                                debug_count_crc ^= symvol;
                        }
                         


                        if (CountBufferDebugIn == 3)
                        {
                            if (symvol == 'S')
                                state_DebugRxd = 0;
                            else
                                if (symvol == 'V')
                                    state_DebugRxd = 1;
                                else
                                    if (symvol == 'M')
                                        state_DebugRxd = 2;
                                    else
                                        if (symvol == 'G')
                                            state_DebugRxd = 3;
                                        else
                                            if (symvol == 'L')
                                                state_DebugRxd = 4;
                                            else
                                                if (symvol == 'R')
                                                    state_DebugRxd = 5;
                                                else
                                                    if (symvol == 'T')
                                                        state_DebugRxd = 6;
                                                    else
                                                        if (symvol == 'C')
                                                            state_DebugRxd = 7;
                                                        else
                                                            if (symvol == 'N')
                                                                state_DebugRxd = 8;
                                                            else
                                                                if (symvol == 'B')
                                                                    state_DebugRxd = 9;
                                                                else
                                                                    if (symvol == 'W')
                                                                        state_DebugRxd = 10;
                                                                    else
                                                                        if (symvol == 'Z')
                                                                            state_DebugRxd = 11;
                                                                        else
                                                                            if (symvol == 'D')
                                                                                state_DebugRxd = 12;
                        }

                    }





                    switch (state_DebugRxd)
                    {
                        case 0://'S'
                        case 9://'B'
                            if (MY_DEBUG_MODE == 1)
                            {
                                if (CountBufferDebugIn == 3)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 4)
                                {
                                   // Get_from_nozzle(symvol);
                                    return;
                                }
                            }
                            else
                            {
                                if (CountBufferDebugIn == 5)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 6)
                                {
                                    Get_from_nozzle(symvol, ref CountBufferDebugIn, ref debug_count_crc_stop,
                                    ref debug_count_crc, ref started_DebugRxd, ref state_DebugRxd); 

                                    if(InBuffer[2]!=chanel)
                                    {
                                        //Должны найти объект соответствующий данному каналу
                                        if (nozzreq1.chanel == InBuffer[2])
                                        {
                                            nozzreq1.state = (uint)(InBuffer[4] - '0');
                                            nozzreq1.nozzle = (uint)(InBuffer[5] - '0');
                                            SetPictureBox(ref nozzreq1.chanel, ref nozzreq1.nozzle);
                                        }
                                        if (nozzreq2.chanel == InBuffer[2])
                                        {
                                            nozzreq2.state = (uint)(InBuffer[4] - '0');
                                            nozzreq2.nozzle = (uint)(InBuffer[5] - '0');
                                            SetPictureBox(ref nozzreq2.chanel, ref nozzreq2.nozzle);
                                        }
                                        if (nozzreq3.chanel == InBuffer[2])
                                        {
                                            nozzreq3.state = (uint)(InBuffer[4] - '0');
                                            nozzreq3.nozzle = (uint)(InBuffer[5] - '0');
                                            SetPictureBox(ref nozzreq3.chanel, ref nozzreq3.nozzle);
                                        }
                                        if (nozzreq4.chanel == InBuffer[2])
                                        {
                                            nozzreq4.state = (uint)(InBuffer[4] - '0');
                                            nozzreq4.nozzle = (uint)(InBuffer[5] - '0');
                                            SetPictureBox(ref nozzreq4.chanel, ref nozzreq4.nozzle);
                                        }
                                        if (nozzreq5.chanel == InBuffer[2])
                                        {
                                            nozzreq5.state = (uint)(InBuffer[4] - '0');
                                            nozzreq5.nozzle = (uint)(InBuffer[5] - '0');
                                            SetPictureBox(ref nozzreq5.chanel, ref nozzreq5.nozzle);
                                        }
                                        if (nozzreq6.chanel == InBuffer[2])
                                        {
                                            nozzreq6.state = (uint)(InBuffer[4] - '0');
                                            nozzreq6.nozzle = (uint)(InBuffer[5] - '0');
                                            SetPictureBox(ref nozzreq6.chanel, ref nozzreq6.nozzle);
                                        }
                                        if (nozzreq7.chanel == InBuffer[2])
                                        {
                                            nozzreq7.state = (uint)(InBuffer[4] - '0');
                                            nozzreq7.nozzle = (uint)(InBuffer[5] - '0');
                                            SetPictureBox(ref nozzreq7.chanel, ref nozzreq7.nozzle);
                                        }
                                        if (nozzreq8.chanel == InBuffer[2])
                                        {
                                            nozzreq8.state = (uint)(InBuffer[4] - '0');
                                            nozzreq8.nozzle = (uint)(InBuffer[5] - '0');
                                            SetPictureBox(ref nozzreq8.chanel, ref nozzreq8.nozzle);
                                        }
                                        CountBufferDebugIn = 0;
                                        serialPort1.ReadTimeout = 70;
                                        ReadFunct(ref CountBufferDebugIn, ref InBuffer, ref chanel, ref  state, ref nozzle);
                                    }
                                    else
                                    {
                                        state = (uint)(InBuffer[4] - '0');
                                        nozzle = (uint)(InBuffer[5] - '0');
                                        SetPictureBox(ref chanel, ref nozzle);
                                    }


                                   // void LabelRefresh(NozzleRequest nozzreq, uint CurrentChanel)
                                        // {
                                           // label20.Text = nozzreq.lable20;
            
                                           // label3.Text = CurrentChanel.ToString();
                                         //}

                                    serialPort1.ReadTimeout = 70;
                                    return;
                                }

                            }

                            break;
                        case 1://'V'
                        case 2://'M'
                            if (MY_DEBUG_MODE == 1)
                            {
                                if (CountBufferDebugIn == 16)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 17)
                                {
                                    //Get_from_nozzle(symvol);  //в этой точке data контрольная сумма       
                                    return;
                                }
                            }

                            break;
                        case 3://'G'
                        case 8://'N'
                            if (MY_DEBUG_MODE == 1)
                            {
                                if (CountBufferDebugIn == 3)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 4)
                                {
                                    //Get_from_nozzle(symvol);  //в этой точке data контрольная сумма       
                                    return;
                                }
                            }
                            break;
                        case 4://'L'
                        case 5://'R'  
                            if (MY_DEBUG_MODE == 1)
                            {
                                if (CountBufferDebugIn == 3)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 4)
                                {
                                   // Get_from_nozzle(symvol);  //в этой точке data контрольная сумма       
                                    return;
                                }
                            }
                            else
                            {
                                if (CountBufferDebugIn == 13)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 14)
                                {
                                    Get_from_nozzle(symvol, ref CountBufferDebugIn, ref debug_count_crc_stop,
                                    ref debug_count_crc, ref started_DebugRxd, ref state_DebugRxd);

                                   

                                    if (InBuffer[2] != chanel)
                                    {
                                        if (InBuffer[2] == 1)
                                            nozzreqTemp = nozzreq1;
                                        if (InBuffer[2] == 2)
                                            nozzreqTemp = nozzreq2;
                                        if (InBuffer[2] == 3)
                                            nozzreqTemp = nozzreq3;
                                        if (InBuffer[2] == 4)
                                            nozzreqTemp = nozzreq4;
                                        if (InBuffer[2] == 5)
                                            nozzreqTemp = nozzreq5;
                                        if (InBuffer[2] == 6)
                                            nozzreqTemp = nozzreq6;
                                        if (InBuffer[2] == 7)
                                            nozzreqTemp = nozzreq7;
                                        if (InBuffer[2] == 8)
                                            nozzreqTemp = nozzreq8;
                                        TR_L_Command(ref InBuffer, ref CountBufferDebugIn, nozzreqTemp, ref chanel);
                                        TR_R_Command(ref InBuffer, ref CountBufferDebugIn, nozzreqTemp, ref chanel);
                                        CountBufferDebugIn = 0;
                                        serialPort1.ReadTimeout = 70;
                                        ReadFunct(ref CountBufferDebugIn, ref InBuffer, ref chanel, ref  state, ref nozzle);
                                        
                                    }
                                    else
                                    {
                                        if (InBuffer[2] == 1)
                                            nozzreqTemp = nozzreq1;
                                        if (InBuffer[2] == 2)
                                            nozzreqTemp = nozzreq2;
                                        if (InBuffer[2] == 3)
                                            nozzreqTemp = nozzreq3;
                                        if (InBuffer[2] == 4)
                                            nozzreqTemp = nozzreq4;
                                        if (InBuffer[2] == 5)
                                            nozzreqTemp = nozzreq5;
                                        if (InBuffer[2] == 6)
                                            nozzreqTemp = nozzreq6;
                                        if (InBuffer[2] == 7)
                                            nozzreqTemp = nozzreq7;
                                        if (InBuffer[2] == 8)
                                            nozzreqTemp = nozzreq8;
                                        TR_L_Command(ref InBuffer, ref CountBufferDebugIn, nozzreqTemp, ref chanel);
                                        TR_R_Command(ref InBuffer, ref CountBufferDebugIn, nozzreqTemp, ref chanel);
                                    }

                                    serialPort1.ReadTimeout = 70;
                                    return;
                                }
                            }

                            break;
                        case 6://'T'
                            if (MY_DEBUG_MODE == 1)
                            {
                                if (CountBufferDebugIn == 3)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 4)
                                {
                                    //Get_from_nozzle(symvol);  //в этой точке data контрольная сумма       
                                    return;
                                }
                            }
                            else
                            {
                                if (CountBufferDebugIn == 25)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 26)
                                {
                                    Get_from_nozzle(symvol, ref CountBufferDebugIn, ref debug_count_crc_stop,
                                     ref debug_count_crc, ref started_DebugRxd, ref state_DebugRxd);
                                    if (InBuffer[2] != chanel)
                                    {
                                        if (InBuffer[2] == 1)
                                            nozzreqTemp = nozzreq1;
                                        if (InBuffer[2] == 2)
                                            nozzreqTemp = nozzreq2;
                                        if (InBuffer[2] == 3)
                                            nozzreqTemp = nozzreq3;
                                        if (InBuffer[2] == 4)
                                            nozzreqTemp = nozzreq4;
                                        if (InBuffer[2] == 5)
                                            nozzreqTemp = nozzreq5;
                                        if (InBuffer[2] == 6)
                                            nozzreqTemp = nozzreq6;
                                        if (InBuffer[2] == 7)
                                            nozzreqTemp = nozzreq7;
                                        if (InBuffer[2] == 8)
                                            nozzreqTemp = nozzreq8;
                                        TR_T_Command(ref InBuffer, ref CountBufferDebugIn, nozzreqTemp, ref chanel);
                                       
                                        CountBufferDebugIn = 0;
                                        serialPort1.ReadTimeout = 70;
                                        ReadFunct(ref CountBufferDebugIn, ref InBuffer, ref chanel, ref  state, ref nozzle);

                                    }
                                    else
                                    {
                                        if (InBuffer[2] == 1)
                                            nozzreqTemp = nozzreq1;
                                        if (InBuffer[2] == 2)
                                            nozzreqTemp = nozzreq2;
                                        if (InBuffer[2] == 3)
                                            nozzreqTemp = nozzreq3;
                                        if (InBuffer[2] == 4)
                                            nozzreqTemp = nozzreq4;
                                        if (InBuffer[2] == 5)
                                            nozzreqTemp = nozzreq5;
                                        if (InBuffer[2] == 6)
                                            nozzreqTemp = nozzreq6;
                                        if (InBuffer[2] == 7)
                                            nozzreqTemp = nozzreq7;
                                        if (InBuffer[2] == 8)
                                            nozzreqTemp = nozzreq8;
                                        TR_T_Command(ref InBuffer, ref CountBufferDebugIn, nozzreqTemp, ref chanel);
                                        
                                    }


                                    serialPort1.ReadTimeout = 70;

                                    return;
                                }
                            }

                            break;
                        case 7://'C'
                            if (MY_DEBUG_MODE == 1)
                            {
                                if (CountBufferDebugIn == 4)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 5)
                                {
                                    //Get_from_nozzle(symvol);  //в этой точке data контрольная сумма       
                                    return;
                                }
                            }
                            else
                            {
                                if (CountBufferDebugIn == 14)
                                {
                                    debug_count_crc_stop = 1;
                                }
                                if (CountBufferDebugIn == 15)
                                {
                                    Get_from_nozzle(symvol, ref CountBufferDebugIn, ref debug_count_crc_stop,
                                      ref debug_count_crc, ref started_DebugRxd, ref state_DebugRxd);

                                    if (InBuffer[2] != chanel)
                                    {
                                        if (InBuffer[2] == 1)
                                            nozzreqTemp = nozzreq1;
                                        if (InBuffer[2] == 2)
                                            nozzreqTemp = nozzreq2;
                                        if (InBuffer[2] == 3)
                                            nozzreqTemp = nozzreq3;
                                        if (InBuffer[2] == 4)
                                            nozzreqTemp = nozzreq4;
                                        if (InBuffer[2] == 5)
                                            nozzreqTemp = nozzreq5;
                                        if (InBuffer[2] == 6)
                                            nozzreqTemp = nozzreq6;
                                        if (InBuffer[2] == 7)
                                            nozzreqTemp = nozzreq7;
                                        if (InBuffer[2] == 8)
                                            nozzreqTemp = nozzreq8;
                                        TR_C_Command(ref InBuffer, ref CountBufferDebugIn, nozzreqTemp, ref chanel);

                                        CountBufferDebugIn = 0;
                                        serialPort1.ReadTimeout = 70;
                                        ReadFunct(ref CountBufferDebugIn, ref InBuffer, ref chanel, ref  state, ref nozzle);

                                    }
                                    else
                                    {
                                        if (InBuffer[2] == 1)
                                            nozzreqTemp = nozzreq1;
                                        if (InBuffer[2] == 2)
                                            nozzreqTemp = nozzreq2;
                                        if (InBuffer[2] == 3)
                                            nozzreqTemp = nozzreq3;
                                        if (InBuffer[2] == 4)
                                            nozzreqTemp = nozzreq4;
                                        if (InBuffer[2] == 5)
                                            nozzreqTemp = nozzreq5;
                                        if (InBuffer[2] == 6)
                                            nozzreqTemp = nozzreq6;
                                        if (InBuffer[2] == 7)
                                            nozzreqTemp = nozzreq7;
                                        if (InBuffer[2] == 8)
                                            nozzreqTemp = nozzreq8;
                                        TR_C_Command(ref InBuffer, ref CountBufferDebugIn, nozzreqTemp, ref chanel);

                                    }

                                    serialPort1.ReadTimeout = 70;

                                    return;
                                }
                            }

                            break;
                        case 10:

                            break;
                        case 11:

                            break;
                        case 12:

                            break;
                        //default:
                        // return 1;
                    }
                }
            }
            catch (TimeoutException)
            {
                //MessageBox.Show("Не удалось получить ответ");
            }

        }

        public void Get_from_nozzle(byte symvol, ref byte CountBufferDebugIn, ref byte debug_count_crc_stop,
            ref byte debug_count_crc, ref byte started_DebugRxd, ref byte state_DebugRxd)
           {
                debug_count_crc_stop=0;
                     if(debug_count_crc==symvol)//контрольная сумма совпадает 
                      {
           
                         
                         started_DebugRxd=0;
                         state_DebugRxd=0xFF;
             
                         
                         //CountBufferDebugIn=0;
                      }
           }
        public void TR_L_Command(ref byte[] InBuffer, ref byte CountBufferDebugIn, NozzleRequest nozzreq, ref byte chanel)
        {
            byte receivedcrc;
            byte countreceivedcrc;

            if (CountBufferDebugIn != 0xFF)//если данные пришли надо вывести в окошко
            {
                result = 0;
                countreceivedcrc = CountCRC(InBuffer, CountBufferDebugIn, result);
                receivedcrc = InBuffer[14];
                if ((countreceivedcrc == receivedcrc) && (InBuffer[3] == 'L'))
                {
                    nozzreq.nozzle = (uint)(InBuffer[4] - '0');
                    nozzreq.TransactionID = (char)(InBuffer[5]);
                    nozzreq.state = (uint)(InBuffer[6] - '0');

                    uint u = 0;
                    uint i;
                    byte[] chararray = { 0x00, 0x00, 0x00, 0x00 };
                    for (i = 0; i < 4; i++)//поиск первого значимого числа
                    {
                        if (InBuffer[8 + i] > '0')
                        {
                            chararray[u] = InBuffer[8 + i];
                            u++;
                            break;
                        }
                    }
                    uint v;
                    for (v = i + 1; v < 4; v++)//дописываем
                    {
                        chararray[u] = InBuffer[8 + v];
                        u++;
                    }
                    //здесь u получившееся количество знаков
                    char[] wholeArr = new char[u];
                    for (int z = 0; z < u; z++)
                    {
                        wholeArr[z] = (char)chararray[z];
                    }
                    string wholestr = new string(wholeArr);//получили в строке целую часть

                    char[] drobArr = new char[2];
                    drobArr[0] = (char)InBuffer[12];
                    drobArr[1] = (char)InBuffer[13];
                    string drobstr = new string(drobArr);//получили в строке дробную часть

                    //label14.Text = state.ToString("X");
                    if (CurrentChanel == chanel)
                    {
                        //label14.Text = wholestr;
                        //label29.Invoke(new Action<string>(t => label29.Text = t), wholestr);
                        label14.Invoke(new Action<string>(t => label14.Text = t), wholestr);
                        //label16.Text = drobstr;
                        label16.Invoke(new Action<string>(t => label16.Text = t), drobstr);
                    }
                    nozzreq.lable14 = wholestr;
                    nozzreq.lable16 = drobstr;
                    lock (Form1.locker)
                    {
                        // запись в файл
                        string buildnote = "logfile";
                        string buildchanel = chanel.ToString();
                        string buildend = ".txt";
                        string buildsum = buildnote + buildchanel + buildend;

                        StreamWriter sw1;
                        sw1 = File.AppendText(buildsum);
                        sw1.WriteLine("получен ответ RT_V команду" + "  " + DateTime.Now);
                        sw1.Close();
                    }



                }

            }


        }
        public void TR_R_Command(ref byte[] InBuffer, ref byte CountBufferDebugIn, NozzleRequest nozzreq, ref byte chanel)
        {
            byte receivedcrc;
            byte countreceivedcrc;
            if (CountBufferDebugIn != 0xFF)//если данные пришли надо вывести в окошко
            {
                result = 0;
                countreceivedcrc = CountCRC(InBuffer, CountBufferDebugIn, result);
                receivedcrc = InBuffer[14];
                if ((countreceivedcrc == receivedcrc) && (InBuffer[3] == 'R'))
                {
                    nozzreq.nozzle = (uint)(InBuffer[4] - '0');
                    nozzreq.TransactionID = (char)(InBuffer[5]);
                    nozzreq.state = (uint)(InBuffer[6] - '0');

                    uint u = 0;
                    uint i;
                    byte[] chararray = { 0x00, 0x00, 0x00, 0x00 };
                    for (i = 0; i < 4; i++)//поиск первого значимого числа
                    {
                        if (InBuffer[8 + i] > '0')
                        {
                            chararray[u] = InBuffer[8 + i];
                            u++;
                            break;
                        }
                    }
                    uint v;
                    for (v = i + 1; v < 4; v++)//дописываем
                    {
                        chararray[u] = InBuffer[8 + v];
                        u++;
                    }
                    //здесь u получившееся количество знаков
                    char[] wholeArr = new char[u];
                    for (int z = 0; z < u; z++)
                    {
                        wholeArr[z] = (char)chararray[z];
                    }
                    string wholestr = new string(wholeArr);//получили в строке целую часть

                    char[] drobArr = new char[2];
                    drobArr[0] = (char)InBuffer[12];
                    drobArr[1] = (char)InBuffer[13];
                    string drobstr = new string(drobArr);//получили в строке дробную часть

                    //label14.Text = state.ToString("X");
                    if (CurrentChanel == chanel)
                    {
                        //label17.Text = wholestr;
                        label17.Invoke(new Action<string>(t => label17.Text = t), wholestr + drobstr);
                        //label19.Text = drobstr;
                        //label19.Invoke(new Action<string>(t => label19.Text = t), drobstr);
                    }
                    string buildsum = wholestr + drobstr;
                    nozzreq.lable17 = buildsum;
                    //nozzreq.lable19 = drobstr;
                    lock (Form1.locker)
                    {
                        // запись в файл
                        string buildnote = "logfile";
                        string buildchanel = chanel.ToString();
                        string buildend = ".txt";
                        string buildsum1 = buildnote + buildchanel + buildend;

                        StreamWriter sw2;
                        sw2 = File.AppendText(buildsum1);
                        sw2.WriteLine("получен ответ на RT_М команду" + "  " + DateTime.Now);
                        sw2.Close();
                    }

                }

            }
        }
        public void TR_T_Command(ref byte[] InBuffer, ref byte CountBufferDebugIn, NozzleRequest nozzreq, ref byte chanel)
        {
            byte receivedcrc;
            byte countreceivedcrc;
            if (CountBufferDebugIn != 0xFF)//если данные пришли надо вывести в окошко
            {
                result = 0;
                countreceivedcrc = CountCRC(InBuffer, CountBufferDebugIn, result);
                receivedcrc = InBuffer[26];
                if ((countreceivedcrc == receivedcrc) && (InBuffer[3] == 'T'))
                {
                    nozzreq.nozzle = (uint)(InBuffer[4] - '0');
                    nozzreq.TransactionID = (char)(InBuffer[5]);
                    nozzreq.state = (uint)(InBuffer[6] - '0');

                    uint u = 0;
                    uint i;
                    uint v;
                    byte[] chararray = { 0x00, 0x00, 0x00, 0x00 };
                    for (i = 0; i < 4; i++)//поиск первого значимого числа
                    {
                        if (InBuffer[8 + i] > '0')
                        {
                            chararray[u] = InBuffer[8 + i];
                            u++;
                            break;
                        }
                    }
                    //uint v;
                    for (v = i + 1; v < 4; v++)//дописываем
                    {
                        chararray[u] = InBuffer[8 + v];
                        u++;
                    }
                    //здесь u получившееся количество знаков
                    char[] wholeArr = new char[u];
                    for (int z = 0; z < u; z++)
                    {
                        wholeArr[z] = (char)chararray[z];
                    }
                    string wholestr = new string(wholeArr);//получили в строке целую часть

                    char[] drobArr = new char[2];
                    drobArr[0] = (char)InBuffer[12];
                    drobArr[1] = (char)InBuffer[13];
                    string drobstr = new string(drobArr);//получили в строке дробную часть

                    //label14.Text = state.ToString("X");
                    if (CurrentChanel == chanel)
                    {
                        //label20.Text = wholestr;
                        label20.Invoke(new Action<string>(t => label20.Text = t), wholestr + drobstr);
                        //label22.Text = drobstr;
                        //label22.Invoke(new Action<string>(t => label22.Text = t), drobstr);
                    }
                    string buildsum = wholestr + drobstr;
                    nozzreq.lable20 = buildsum;

                    if (CurrentChanel == nozzreq.chanel)//если отображаемый на экране канал равен каналу запоздавшего сообщения то мы его отображаем
                        label20.Invoke(new Action<string>(t => label20.Text = t), wholestr + drobstr);
                    //nozzreq.lable22 = drobstr;

                    u = 0;
                    i = 0;
                    v = 0;

                    for (i = 0; i < 4; i++)//поиск первого значимого числа
                    {
                        if (InBuffer[15 + i] > '0')
                        {
                            chararray[u] = InBuffer[15 + i];
                            u++;
                            break;
                        }
                    }
                    //uint v;
                    for (v = i + 1; v < 4; v++)//дописываем
                    {
                        chararray[u] = InBuffer[15 + v];
                        u++;
                    }
                    //здесь u получившееся количество знаков
                    char[] wholeArr2 = new char[u];
                    for (int z = 0; z < u; z++)
                    {
                        wholeArr2[z] = (char)chararray[z];
                    }
                    string wholestr2 = new string(wholeArr2);//получили в строке целую часть

                    char[] drobArr2 = new char[2];
                    drobArr2[0] = (char)InBuffer[19];
                    drobArr2[1] = (char)InBuffer[20];
                    string drobstr2 = new string(drobArr2);//получили в строке дробную часть
                    if (CurrentChanel == chanel)
                    {
                        //label23.Text = wholestr2;
                        label23.Invoke(new Action<string>(t => label23.Text = t), wholestr2);
                        //label25.Text = drobstr2;
                        label25.Invoke(new Action<string>(t => label25.Text = t), drobstr2);
                    }
                    nozzreq.lable23 = wholestr2;
                    nozzreq.lable25 = drobstr2;

                    if (CurrentChanel == nozzreq.chanel)//если отображаемый на экране канал равен каналу запоздавшего сообщения то мы его отображаем
                    {
                        label23.Invoke(new Action<string>(t => label23.Text = t), wholestr2);                      
                        label25.Invoke(new Action<string>(t => label25.Text = t), drobstr2);
                    }

                    u = 0;
                    i = 0;
                    v = 0;

                    for (i = 0; i < 2; i++)//поиск первого значимого числа
                    {
                        if (InBuffer[22 + i] > '0')
                        {
                            chararray[u] = InBuffer[22 + i];
                            u++;
                            break;
                        }
                    }

                    for (v = i + 1; v < 2; v++)//дописываем
                    {
                        chararray[u] = InBuffer[22 + v];
                        u++;
                    }
                    //здесь u получившееся количество знаков
                    char[] wholeArr3 = new char[u];
                    for (int z = 0; z < u; z++)
                    {
                        wholeArr3[z] = (char)chararray[z];
                    }
                    string wholestr3 = new string(wholeArr3);//получили в строке целую часть

                    char[] drobArr3 = new char[2];
                    drobArr3[0] = (char)InBuffer[24];
                    drobArr3[1] = (char)InBuffer[25];
                    string drobstr3 = new string(drobArr3);//получили в строке дробную часть

                    if (CurrentChanel == chanel)
                    {
                        //label26.Text = wholestr3;
                        label26.Invoke(new Action<string>(t => label26.Text = t), wholestr3 + drobstr3);
                        //label28.Text = drobstr3;
                        //label28.Invoke(new Action<string>(t => label28.Text = t), drobstr3);
                    }
                    nozzreq.lable26 = wholestr3 + drobstr3;
                    //nozzreq.lable28 = drobstr3;

                    buildsum = wholestr3 + drobstr3;
                    nozzreq.lable26 = buildsum;

                    if (CurrentChanel == nozzreq.chanel)//если отображаемый на экране канал равен каналу запоздавшего сообщения то мы его отображаем
                        label26.Invoke(new Action<string>(t => label26.Text = t), wholestr3 + drobstr3);

                    lock (Form1.locker)
                    {
                        // запись в файл
                        string buildnote = "logfile";
                        string buildchanel = chanel.ToString();
                        string buildend = ".txt";
                        string buildsum1 = buildnote + buildchanel + buildend;

                        StreamWriter sw10;
                        sw10 = File.AppendText(buildsum1);
                        sw10.WriteLine("получен ответ на Request_Transaction_Data команду" + "  " + DateTime.Now);
                        sw10.Close();
                    }




                }

            }
        }
        public void TR_C_Command(ref byte[] InBuffer, ref byte CountBufferDebugIn, NozzleRequest nozzreq, ref byte chanel)
        {
            byte receivedcrc;
            byte countreceivedcrc;
            if (CountBufferDebugIn != 0xFF)//если данные пришли надо вывести в окошко
            {
                result = 0;
                countreceivedcrc = CountCRC(InBuffer, CountBufferDebugIn, result);
                receivedcrc = InBuffer[15];
                if ((countreceivedcrc == receivedcrc) && (InBuffer[3] == 'C'))
                {
                    nozzreq.nozzle = (uint)(InBuffer[4] - '0');
                    //TransactionID = (char)(InBuffer[5]);
                    //state = (uint)(InBuffer[6] - '0');

                    uint u = 0;
                    uint i;
                    byte[] chararray = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
                    for (i = 0; i < 7; i++)//поиск первого значимого числа
                    {
                        if (InBuffer[6 + i] > '0')
                        {
                            chararray[u] = InBuffer[6 + i];
                            u++;
                            break;
                        }
                    }
                    uint v;
                    for (v = i + 1; v < 7; v++)//дописываем
                    {
                        chararray[u] = InBuffer[6 + v];
                        u++;
                    }
                    //здесь u получившееся количество знаков
                    char[] wholeArr = new char[u];
                    for (int z = 0; z < u; z++)
                    {
                        wholeArr[z] = (char)chararray[z];
                    }
                    string wholestr = new string(wholeArr);//получили в строке целую часть

                    char[] drobArr = new char[2];
                    drobArr[0] = (char)InBuffer[13];
                    drobArr[1] = (char)InBuffer[14];
                    string drobstr = new string(drobArr);//получили в строке дробную часть

                    //label14.Text = state.ToString("X");
                    if (CurrentChanel == chanel)
                    {
                        
                            //label29.Text = wholestr;
                        label29.Invoke(new Action<string>(t => label29.Text = t), wholestr);
                            //label31.Text = drobstr;
                        label31.Invoke(new Action<string>(t => label31.Text = t), drobstr);
                       
                    }
                    nozzreq.lable29 = wholestr;
                    nozzreq.lable31 = drobstr;
                    lock (Form1.locker)
                    {
                        // запись в файл
                        string buildnote = "logfile";
                        string buildchanel = chanel.ToString();
                        string buildend = ".txt";
                        string buildsum = buildnote + buildchanel + buildend;

                        StreamWriter sw11;
                        sw11 = File.AppendText(buildsum);
                        sw11.WriteLine("получен ответ на Request_Total_Counter команду" + "  " + DateTime.Now);
                        sw11.Close();
                    }



                }

            }
        }


    }
}
