﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        byte result;
        public void Start_Transaction(ref byte chanel, ref uint state, ref uint nozzle, ref long Transaction_Money, ref uint Transaction_Liters, ref long Price_for_Liter)
        {
            //сначала необходимо спарсить данные о сделке
            //но сначала нужно понять какое окно активно и есть ли дам данные

            Transaction_Liters = 0;
            Transaction_Money = 0;

            if ((textBox3.Enabled == true) && (textBox4.Enabled == false))
            {
                try
                {
                    Transaction_Liters = uint.Parse(textBox3.Text);
                }
                catch (ArgumentException)
                {
                    Transaction_Liters = 0;
                }
                catch (FormatException)
                {
                    Transaction_Liters = 0;
                }
                catch (OverflowException)
                {
                    Transaction_Liters = 0;
                }

            }
            else
                if ((textBox4.Enabled == true) && (textBox3.Enabled == false))
                {
                    try
                    {
                        Transaction_Money = uint.Parse(textBox4.Text);
                    }
                    catch (ArgumentException)
                    {
                        Transaction_Money = 0;
                    }
                    catch (FormatException)
                    {
                        Transaction_Money = 0;
                    }
                    catch (OverflowException)
                    {
                        Transaction_Money = 0;
                    }

                }
                else
                    if ((textBox4.Enabled == true) && (textBox3.Enabled == true))
                    {
                        Transaction_Money = 0;
                        Transaction_Liters = 0;
                    }


            if (Transaction_Liters > 0)
            {
                //string str;
                char command = 'V';
                byte byte2 = chanel;
                byte byte3 = (byte)command;
                byte byte4 = 0x31;//первое сопло, здесь величина постоянная
                byte crc;
                byte delimiter = (byte)';';
                long tr = Transaction_Liters * 100;//отправляться будет в санцилитрах

                //long a;
                //a = long.Parse(textBox1.Text);
                str = tr.ToString();

                byte ln = (byte)str.Length;//определили длину строки
                if (ln > 6)
                    return;

                byte[] InBuffer = new byte[255];
                byte CounterReceive = 0xFF;
               // byte receivedcrc;
               // byte countreceivedcrc;


                byte[] array = { 0x02, 0x00, byte2, byte3, byte4, delimiter, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, delimiter, 0x30, 0x30, 0x30, 0x30, 0x30 };    // V
                if ((ln - 1) >= 0)
                    array[11] = (byte)str[ln - 1];
                if ((ln - 2) >= 0)
                    array[10] = (byte)str[ln - 2];
                if ((ln - 3) >= 0)
                    array[9] = (byte)str[ln - 3];
                if ((ln - 4) >= 0)
                    array[8] = (byte)str[ln - 4];
                if ((ln - 5) >= 0)
                    array[7] = (byte)str[ln - 5];
                if ((ln - 6) >= 0)
                    array[6] = (byte)str[ln - 6];

                //Price_for_Liter
                long pr = Price_for_Liter;
                str = pr.ToString();
                ln = (byte)str.Length;//определили длину строки
                if (ln > 4)
                    return;
                if ((ln - 1) >= 0)
                    array[16] = (byte)str[ln - 1];
                if ((ln - 2) >= 0)
                    array[15] = (byte)str[ln - 2];
                if ((ln - 3) >= 0)
                    array[14] = (byte)str[ln - 3];
                if ((ln - 4) >= 0)
                    array[13] = (byte)str[ln - 4];


                result = 0;
                crc = CountCRC(array, 17, result);
                array[17] = crc;
                serialPort1.Write(array, 0, 18);

                ReadFunct(ref CounterReceive, ref InBuffer, ref chanel, ref  state, ref  nozzle);

                if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
                {
                    
                }
                else
                {
                    lock (Form1.locker)
                    {
                        // запись в файл
                        string buildnote = "logfile";
                        string buildchanel = chanel.ToString();
                        string buildend = ".txt";
                        string buildsum = buildnote + buildchanel + buildend;

                        StreamWriter sw;
                        sw = File.AppendText(buildsum);
                        sw.WriteLine("ответ на старт сделки не получили" + "  " + DateTime.Now);
                        sw.Close();
                    }

                }
            }//конец if (Transaction_Liters > 0)
            else
            if (Transaction_Money > 0)
            {
                //string str;
                char command = 'M';
                byte byte2 = chanel;
                byte byte3 = (byte)command;
                byte byte4 = 0x31;//первое сопло, здесь величина постоянная
                byte crc;
                byte delimiter = (byte)';';
                long tr = Transaction_Money;//

                //long a;
                //a = long.Parse(textBox1.Text);
                str = tr.ToString();

                byte ln = (byte)str.Length;//определили длину строки
                if (ln > 6)
                    return;

                byte[] InBuffer = new byte[255];
                byte CounterReceive = 0xFF;
                //byte receivedcrc;
                //byte countreceivedcrc;


                byte[] array = { 0x02, 0x00, byte2, byte3, byte4, delimiter, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, delimiter, 0x30, 0x30, 0x30, 0x30, 0x30 };    // M
                if ((ln - 1) >= 0)
                    array[11] = (byte)str[ln - 1];
                if ((ln - 2) >= 0)
                    array[10] = (byte)str[ln - 2];
                if ((ln - 3) >= 0)
                    array[9] = (byte)str[ln - 3];
                if ((ln - 4) >= 0)
                    array[8] = (byte)str[ln - 4];
                if ((ln - 5) >= 0)
                    array[7] = (byte)str[ln - 5];
                if ((ln - 6) >= 0)
                    array[6] = (byte)str[ln - 6];

                //Price_for_Liter
                long pr = Price_for_Liter;
                str = pr.ToString();
                ln = (byte)str.Length;//определили длину строки
                if (ln > 4)
                    return;
                if ((ln - 1) >= 0)
                    array[16] = (byte)str[ln - 1];
                if ((ln - 2) >= 0)
                    array[15] = (byte)str[ln - 2];
                if ((ln - 3) >= 0)
                    array[14] = (byte)str[ln - 3];
                if ((ln - 4) >= 0)
                    array[13] = (byte)str[ln - 4];


                result = 0;
                crc = CountCRC(array, 17, result);
                array[17] = crc;
                serialPort1.Write(array, 0, 18);

                ReadFunct(ref CounterReceive, ref InBuffer, ref chanel, ref  state, ref  nozzle);

                if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
                {
                    
                }
                else
                {
                    lock (Form1.locker)
                    {
                        // запись в файл
                        string buildnote = "logfile";
                        string buildchanel = chanel.ToString();
                        string buildend = ".txt";
                        string buildsum = buildnote + buildchanel + buildend;

                        StreamWriter sw;
                        sw = File.AppendText(buildsum);
                        sw.WriteLine("ответ на старт сделки не получили" + "  " + DateTime.Now);
                        sw.Close();
                    }

                }
            }//конец if (Transaction_Money > 0)

        }//Start_Transaction()

       
        public void Ask_Status(ref byte chanel, ref uint state, ref uint nozzle)
        {
            
            byte mystop=0;

            char status = 'S';
            byte byte2 = chanel;
            byte byte3 = (byte)status;
            byte byte4 = 0;
            byte[] InBuffer = new byte[255];
            byte CounterReceive = 0xFF;
            //byte receivedcrc;
            //byte countreceivedcrc;
      

            byte[] array = { 0x02, 0x00, byte2, byte3, byte4 };    // Status

            result = 0;
            byte4 = CountCRC(array, 4, result);
            array[4] = byte4;

            for (byte i=0; i < 128; i++ )
            {
                InBuffer[i] = 0;

            }
                serialPort1.Write(array, 0, 5);

            if (chanel == 2)
                mystop++;

            ReadFunct(ref CounterReceive, ref InBuffer, ref chanel,  ref  state, ref  nozzle);

            if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
            {
              
            }
            else
            {
                lock (Form1.locker)
                {
                    // запись в файл
                    string buildnote = "logfile";
                    string buildchanel = chanel.ToString();
                    string buildend = ".txt";
                    string buildsum = buildnote + buildchanel + buildend;

                    StreamWriter sw2;
                    sw2 = File.AppendText(buildsum);
                    sw2.WriteLine("не получили ответ на статус" + "  " + DateTime.Now.Ticks);
                    sw2.Close();
                }

            }


        }//Ask_Status()

        public void Resume(ref byte chanel, ref uint state, ref uint nozzle)
        {
            char command = 'G';
            byte byte2 = chanel;
            byte byte3 = (byte)command;
            byte byte4 = 0;
            byte[] InBuffer = new byte[255];
            byte CounterReceive = 0xFF;
           // byte receivedcrc;
           // byte countreceivedcrc;


          

            byte[] array = { 0x02, 0x00, byte2, byte3, byte4 };    // Resume

            result = 0;
            byte4 = CountCRC(array, 4, result);
            array[4] = byte4;
            serialPort1.Write(array, 0, 5);

            ReadFunct(ref CounterReceive, ref InBuffer, ref chanel, ref  state, ref  nozzle);

            if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
            {
               
            }
            else
            {
                lock (Form1.locker)
                {
                    // запись в файл
                    string buildnote = "logfile";
                    string buildchanel = chanel.ToString();
                    string buildend = ".txt";
                    string buildsum = buildnote + buildchanel + buildend;

                    StreamWriter sw;
                    sw = File.AppendText(buildsum);
                    sw.WriteLine("не получили ответ на возобновить" + "  " + DateTime.Now);
                    sw.Close();
                }

            }


        }//Resume()

        public void StopCommand(ref byte chanel, ref uint state, ref uint nozzle)
        {
            char command = 'B';
            byte byte2 = chanel;
            byte byte3 = (byte)command;
            byte byte4 = 0;
            byte[] InBuffer = new byte[255];
            byte CounterReceive = 0xFF;
            //byte receivedcrc;
            //byte countreceivedcrc;

            byte[] array = { 0x02, 0x00, byte2, byte3, byte4 };    // Stop

            result = 0;
            byte4 = CountCRC(array, 4, result);
            array[4] = byte4;
            serialPort1.Write(array, 0, 5);

            ReadFunct(ref CounterReceive, ref InBuffer, ref chanel, ref  state, ref  nozzle);

            if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
            {
               
            }
            else
            {
                lock (Form1.locker)
                {
                    // запись в файл
                    string buildnote = "logfile";
                    string buildchanel = chanel.ToString();
                    string buildend = ".txt";
                    string buildsum = buildnote + buildchanel + buildend;

                    StreamWriter sw;
                    sw = File.AppendText(buildsum);
                    sw.WriteLine("не получили ответ на стоп команду" + "  " + DateTime.Now);
                    sw.Close();
                }
            }

        }//Stop()

        public void Request_RT_V(ref byte chanel, ref uint state, ref uint nozzle, ref char TransactionID, ref string lable14, ref string lable16)
        {
            char command = 'L';
            byte byte2 = chanel;
            byte byte3 = (byte)command;
            byte byte4 = 0;
            byte[] InBuffer = new byte[255];
            byte CounterReceive = 0xFF;
            //byte receivedcrc;
            //byte countreceivedcrc;

            byte[] array = { 0x02, 0x00, byte2, byte3, byte4 };    // Stop

            result = 0;
            byte4 = CountCRC(array, 4, result);
            array[4] = byte4;
            serialPort1.Write(array, 0, 5);

            ReadFunct(ref CounterReceive, ref InBuffer, ref chanel, ref  state, ref  nozzle);

            if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
            {
               
            }
            else
            {
                lock (Form1.locker)
                {
                    // запись в файл
                    string buildnote = "logfile";
                    string buildchanel = chanel.ToString();
                    string buildend = ".txt";
                    string buildsum = buildnote + buildchanel + buildend;

                    StreamWriter sw1;
                    sw1 = File.AppendText(buildsum);
                    sw1.WriteLine("не получен ответ на RT_V команду" + "  " + DateTime.Now);
                    sw1.Close();
                }
            }

        }//Request_RT_V()

        public void Request_RT_M(ref byte chanel, ref uint state, ref uint nozzle, ref char TransactionID, ref string lable17/*, ref string lable19*/)
        {
            char command = 'R';
            byte byte2 = chanel;
            byte byte3 = (byte)command;
            byte byte4 = 0;
            byte[] InBuffer = new byte[255];
            byte CounterReceive = 0xFF;
            //byte receivedcrc;
            //byte countreceivedcrc;

            byte[] array = { 0x02, 0x00, byte2, byte3, byte4 };    // Request_RT_M

            result = 0;
            byte4 = CountCRC(array, 4, result);
            array[4] = byte4;
            serialPort1.Write(array, 0, 5);

            ReadFunct(ref CounterReceive, ref InBuffer, ref chanel, ref  state, ref  nozzle);


            if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
            {
               
            }
            else
            {
                lock (Form1.locker)
                {
                    // запись в файл
                    string buildnote = "logfile";
                    string buildchanel = chanel.ToString();
                    string buildend = ".txt";
                    string buildsum = buildnote + buildchanel + buildend;

                    StreamWriter sw2;
                    sw2 = File.AppendText(buildsum);
                    sw2.WriteLine("не получен ответ на RT_М команду" + "  " + DateTime.Now);
                    sw2.Close();
                }
            }

        }//Request_RT_M()

        public void Request_Transaction_Data(ref byte chanel, ref uint state,
        ref string lable20,
        //ref string lable22,
        ref string lable23,
        ref string lable25,
        ref string lable26,
        //ref string lable28, 
            ref uint nozzle, ref char TransactionID)
        {
            char command = 'T';
            byte byte2 = chanel;
            byte byte3 = (byte)command;
            byte byte4 = 0;
            byte[] InBuffer = new byte[255];
            byte CounterReceive = 0xFF;
            byte receivedcrc;
            byte countreceivedcrc;

            byte[] array = { 0x02, 0x00, byte2, byte3, byte4 };    // Transaction_Data

            result = 0;
            byte4 = CountCRC(array, 4, result);
            array[4] = byte4;
            serialPort1.Write(array, 0, 5);

            ReadFunct(ref CounterReceive, ref InBuffer, ref chanel, ref  state, ref  nozzle);
           

            if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
            {
               
            }
            else
            {
                lock (Form1.locker)
                {
                    // запись в файл
                    string buildnote = "logfile";
                    string buildchanel = chanel.ToString();
                    string buildend = ".txt";
                    string buildsum = buildnote + buildchanel + buildend;

                    StreamWriter sw10;
                    sw10 = File.AppendText(buildsum);
                    sw10.WriteLine("не получен ответ на Request_Transaction_Data команду" + "  " + DateTime.Now);
                    sw10.Close();
                }
            }

        }//Request_Transaction_Data()

        public void Request_Total_Counter(ref byte chanel, ref uint nozzle, ref string lable29, ref string lable31, ref uint state)
        {
            char command = 'C';
            byte byte2 = chanel;
            byte byte3 = (byte)command;
            byte crc = 0;
            byte nozzleToSend = 0x01;
            byte[] InBuffer = new byte[255];
            byte CounterReceive = 0xFF;
            //byte receivedcrc;
            //byte countreceivedcrc;

            byte[] array = { 0x02, 0x00, byte2, byte3, nozzleToSend, crc };    // Request_Total_Counter

            result = 0;
            crc = CountCRC(array, 5, result);
            array[5] = crc;
            serialPort1.Write(array, 0, 6);

            ReadFunct(ref CounterReceive, ref InBuffer, ref chanel, ref  state, ref  nozzle);



            if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
            {
               
            }
            else
            {
                lock (Form1.locker)
                {
                    // запись в файл
                    string buildnote = "logfile";
                    string buildchanel = chanel.ToString();
                    string buildend = ".txt";
                    string buildsum = buildnote + buildchanel + buildend;

                    StreamWriter sw11;
                    sw11 = File.AppendText(buildsum);
                    sw11.WriteLine("не получен ответ на Request_Total_Counter команду" + "  " + DateTime.Now);
                    sw11.Close();
                }

            }

        }//Request_Total_Counter

        public void End_Current_Transaction(ref byte chanel, ref uint state, ref uint nozzle, ref uint flag_end_was_sent)
        {
            char command = 'N';
            byte byte2 = chanel;
            byte byte3 = (byte)command;
            byte crc = 0;
            byte[] InBuffer = new byte[255];
            byte CounterReceive = 0xFF;
            //byte receivedcrc;
            //byte countreceivedcrc;




            byte[] array = { 0x02, 0x00, byte2, byte3, crc };    // End_Current_Transaction

            result = 0;
            crc = CountCRC(array, 4, result);
            array[4] = crc;
            serialPort1.Write(array, 0, 5);
            //button8.Enabled = true;

            ReadFunct(ref CounterReceive, ref InBuffer, ref chanel, ref  state, ref  nozzle);

            if (CounterReceive != 0xFF)//если данные пришли надо вывести в окошко
            {
               
                lock (Form1.locker)
                {
                    // запись в файл
                    string buildnote = "logfile";
                    string buildchanel = chanel.ToString();
                    string buildend = ".txt";
                    string buildsum = buildnote + buildchanel + buildend;

                    StreamWriter sw7;
                    sw7 = File.AppendText(buildsum);
                    sw7.WriteLine("получили ответ на енд команду" + "  " + DateTime.Now);
                    sw7.Close();
                }
                flag_end_was_sent = 1;

            }
            else
            {
                lock (Form1.locker)
                {
                    // запись в файл
                    string buildnote = "logfile";
                    string buildchanel = chanel.ToString();
                    string buildend = ".txt";
                    string buildsum = buildnote + buildchanel + buildend;

                    StreamWriter sw7;
                    sw7 = File.AppendText(buildsum);
                    sw7.WriteLine("не получили ответ на енд команду" + "  " + DateTime.Now);
                    sw7.Close();
                }

            }


        }//End_Current_Transaction

        public void SetPictureBox(ref byte chanel, ref uint nozzle)
        {
            if (IsTRKExist == 1)
            {
                IsTRKExist = 0;
                TRKExist = 1;
            }
            if (chanel == 1)
            {
                if (nozzle == 0x00)
                    pictureBox1.Image = Image.FromFile("pistolwhite.jpg");
                else
                    //pictureBox1.BackColor = Color.Red;
                    pictureBox1.Image = Image.FromFile("pistol.jpg");
            }
            if (chanel == 2)
            {
                if (nozzle == 0x00)
                    pictureBox2.Image = Image.FromFile("pistolwhite.jpg");
                else
                    //pictureBox2.BackColor = Color.Red;
                    pictureBox2.Image = Image.FromFile("pistol.jpg");
            }
            if (chanel == 3)
            {
                if (nozzle == 0x00)
                    pictureBox3.Image = Image.FromFile("pistolwhite.jpg");
                else
                    //pictureBox3.BackColor = Color.Red;
                    pictureBox3.Image = Image.FromFile("pistol.jpg");
            }
            if (chanel == 4)
            {
                if (nozzle == 0x00)
                    pictureBox4.Image = Image.FromFile("pistolwhite.jpg");
                else
                    //pictureBox4.BackColor = Color.Red;
                    pictureBox4.Image = Image.FromFile("pistol.jpg");
            }
            if (chanel == 5)
            {
                if (nozzle == 0x00)
                    pictureBox5.Image = Image.FromFile("pistolwhite.jpg");
                else
                    //pictureBox5.BackColor = Color.Red;
                    pictureBox5.Image = Image.FromFile("pistol.jpg");
            }
            if (chanel == 6)
            {
                if (nozzle == 0x00)
                    pictureBox6.Image = Image.FromFile("pistolwhite.jpg");
                else
                    //pictureBox6.BackColor = Color.Red;
                    pictureBox6.Image = Image.FromFile("pistol.jpg");
            }
            if (chanel == 7)
            {
                if (nozzle == 0x00)
                    pictureBox7.Image = Image.FromFile("pistolwhite.jpg");
                else
                    //pictureBox7.BackColor = Color.Red;
                    pictureBox7.Image = Image.FromFile("pistol.jpg");
            }
            if (chanel == 8)
            {
                if (nozzle == 0x00)
                    pictureBox8.Image = Image.FromFile("pistolwhite.jpg");
                else
                    //pictureBox8.BackColor = Color.Red;
                    pictureBox8.Image = Image.FromFile("pistol.jpg");
            }
           
        }

       
    }

}