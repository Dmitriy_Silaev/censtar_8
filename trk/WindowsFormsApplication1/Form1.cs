﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using System.Data.SqlClient;//System.Data.SqlClient

using System.IO;

namespace WindowsFormsApplication1
{

    
    public partial class Form1 : Form
    {
        public uint CurrentChanel = 1;
  
        NozzleRequest nozzreq1 = new NozzleRequest(1);
        NozzleRequest nozzreq2 = new NozzleRequest(2);
        NozzleRequest nozzreq3 = new NozzleRequest(3);
        NozzleRequest nozzreq4 = new NozzleRequest(4);
        NozzleRequest nozzreq5 = new NozzleRequest(5);
        NozzleRequest nozzreq6 = new NozzleRequest(6);
        NozzleRequest nozzreq7 = new NozzleRequest(7);
        NozzleRequest nozzreq8 = new NozzleRequest(8);
        NozzleRequest nozzreqTemp = new NozzleRequest(8);

        ClassLables Lables1 = new ClassLables();
        ClassLables Lables2 = new ClassLables();
        ClassLables Lables3 = new ClassLables();
        ClassLables Lables4 = new ClassLables();
        ClassLables Lables5 = new ClassLables();
        ClassLables Lables6 = new ClassLables();
        ClassLables Lables7 = new ClassLables();
        ClassLables Lables8 = new ClassLables();
       
        public const uint ZeroState = 0;
        public const uint Idle = 1;
        public const uint Calling = 2;
        public const uint Authorised = 3;
        public const uint Started = 4;
        public const uint SuspendedStarted = 5;
        public const uint Fuelling = 6;
        public const uint SuspendedFuelling = 7;
        public const uint Stop = 8;
        public const uint End = 9;
       
        public uint AllowAskChannels = 0;
        public uint NeedToClosePort = 0;
        public uint ComPortExist = 0;
        public uint TRKExist = 0;
        public uint IsTRKExist = 1;
        
        public string str;

        public static object locker = new object();
        public Form1()
        {
            InitializeComponent();
            nozzreq1.frm = this;
            nozzreq2.frm = this;
            nozzreq3.frm = this;
            nozzreq4.frm = this;
            nozzreq5.frm = this;
            nozzreq6.frm = this;
            nozzreq7.frm = this;
            nozzreq8.frm = this;
            nozzreqTemp.frm = this;

            

            Lables1.frml = this;
            Lables2.frml = this;
            Lables3.frml = this;
            Lables4.frml = this;
            Lables5.frml = this;
            Lables6.frml = this;
            Lables7.frml = this;
            Lables8.frml = this;
        }

       
        //Form2 tableForm;
       

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'myDB_1DataSet.Table_1' table. You can move, or remove it, as needed.
            //this.table_1TableAdapter1.Fill(this.myDB_1DataSet.Table_1);
            // TODO: This line of code loads data into the 'myDBDataSet.Table_1' table. You can move, or remove it, as needed.
           // this.table_1TableAdapter.Fill(this.myDBDataSet.Table_1);
            try
            {
                string[] str = System.IO.Ports.SerialPort.GetPortNames();
                //this.listBox1.Items.AddRange(new object[] {"456"});
                listBox1.Items.AddRange(str);
                listBox1.SetSelected(0, true);
                ComPortExist = 1;
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Подключите устройство к порту (нет ком порта)");
                Close();
            }
            if (ComPortExist == 1)
            {
                var pollingThread = new Thread(Polling);
                pollingThread.IsBackground = true;
                pollingThread.Start();
            }

            
            
        }
        private void button4_Click(object sender, EventArgs e)//открыть порт
        {
            //label2.Text
            try
            {
                serialPort1.PortName = listBox1.SelectedItem.ToString();
                serialPort1.Open();
                label18.Text = serialPort1.PortName;
                AllowAskChannels = 1;
                timer1.Enabled = true;
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Порт уже открыт");
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Отказ в доступе к порту");
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Недопустимый аргумент");
            }
        }
        private void button1_Click(object sender, EventArgs e)//закрыть порт
        {
            NeedToClosePort = 1;
            //button4.Enabled = false;
            button1.Enabled = false;


            //serialPort1.Close();
            //label18.Text = " ";
        }

        private void button3_Click(object sender, EventArgs e)//кнопка запуск
        {
            //timer1.Enabled = true;
            //button2.Enabled = true;
            button3.Enabled = false;
        }
       
        private void timer1_Tick(object sender, EventArgs e)
        {
            //timer1.Enabled = false;
            if (TRKExist == 1)
            {
                TRKExist = 0;
                textBox1.Visible = true;
                textBox2.Visible = true;
                button5.Visible = true;
                button7.Visible = true;
                label1.Visible = true;
                label2.Visible = true;
                label9.Visible = true;
                groupBox1.Visible = true;
                groupBox2.Visible = true;
                groupBox4.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = true;
                label12.Visible = true;
                label13.Visible = true;
                label10.Visible = true;
                label11.Visible = true;

                label4.Visible = true;
                label3.Visible = true;

                label14.Visible = true;
                label15.Visible = true;
                label16.Visible = true;
                label17.Visible = true;

                button8.Visible = true;
                button9.Visible = true;
                button10.Visible = true;

                label5.Visible = true;
                label6.Visible = true;
                label7.Visible = true;

                label20.Visible = true;
                label23.Visible = true;
                label24.Visible = true;
                label25.Visible = true;
                label26.Visible = true;

                label8.Visible = true;

                label29.Visible = true;
                label30.Visible = true;
                label31.Visible = true;

                button13.Visible = true;
                button14.Visible = true;
                button15.Visible = true;
                button16.Visible = true;
                button17.Visible = true;
                button18.Visible = true;
                button19.Visible = true;
                button20.Visible = true;

                pictureBox1.Visible = true;
                pictureBox2.Visible = true;
                pictureBox3.Visible = true;
                pictureBox4.Visible = true;
                pictureBox5.Visible = true;
                pictureBox6.Visible = true;
                pictureBox7.Visible = true;
                pictureBox8.Visible = true;

                button1.Visible = false;

                timer1.Enabled = false;



            }
        }

       


        private void Polling()
        {
            while (true)
            {
                if (AllowAskChannels == 1)
                {


                    sens_request(1);

                    sens_request(2);

                    sens_request(3);

                    sens_request(4);

                    sens_request(5);

                    sens_request(6);

                    sens_request(7);

                    sens_request(8);

                    if(NeedToClosePort == 1)
                     {
                        NeedToClosePort = 0;
                        AllowAskChannels = 0;
                        serialPort1.Close();
                        //button4.Enabled = true;
                        //button1.Enabled = true;
                        button1.Invoke(new Action<bool>(t => button1.Enabled = t), true);
                        button4.Invoke(new Action<bool>(t => button4.Enabled = t), true);
                        label18.Invoke(new Action<string>(t => label18.Text = t), " ");
                     }
                   
                }
            }
  
        }

        

       

       /*
        private void button12_Click(object sender, EventArgs e)
        {
            // Show the dialog and get result.
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                label32.Text = file;
            }
        }
      */

        Form1 mainForm;
       

        //byte result;
        


        public byte CountCRC(byte[] array, byte length, byte result)
        {
            result = array[1];
            for (int i = 2; i < length; i++)
            {
                result ^= array[i];
            }
            return result;
        }

        
        private void textBox3_Click(object sender, EventArgs e1)
        {
           // Transaction_Money = 0;
            textBox4.Clear();
            textBox4.Enabled = false;
            textBox3.Enabled = true;
        }
        private void label12_Click(object sender, EventArgs e1)
        {
           // Transaction_Money = 0;
            textBox4.Clear();
            textBox4.Enabled = false;
            textBox3.Enabled = true;
        }
        private void textBox4_Click(object sender, EventArgs e1)
        {
           // Transaction_Liters = 0;
            textBox3.Clear();
            textBox3.Enabled = false;
            textBox4.Enabled = true;
        }
        private void label13_Click(object sender, EventArgs e1)
        {
           // Transaction_Liters = 0;
            textBox3.Clear();
            textBox3.Enabled = false;
            textBox4.Enabled = true;
        }



        private void button7_Click(object sender, EventArgs e1)
        {
            if (CurrentChanel == 1)
               nozzreq1.RefreshDisplayChanel();
            if (CurrentChanel == 2)
                nozzreq2.RefreshDisplayChanel();
            if (CurrentChanel == 3)
                nozzreq3.RefreshDisplayChanel();
            if (CurrentChanel == 4)
                nozzreq4.RefreshDisplayChanel();
            if (CurrentChanel == 5)
                nozzreq5.RefreshDisplayChanel();
            if (CurrentChanel == 6)
                nozzreq6.RefreshDisplayChanel();
            if (CurrentChanel == 7)
                nozzreq7.RefreshDisplayChanel();
            if (CurrentChanel == 8)
                nozzreq8.RefreshDisplayChanel();

        }
        public long TextBox2ToLabel2()
        {
            long a = 0;
            //Price_for_Liter = 0;
            try
            {
                a = long.Parse(textBox2.Text);
                //Price_for_Liter = a;
                label2.Text = a.ToString();
                return a;
            }
            catch
            {
                MessageBox.Show("Введите верное значение суммы");
            }
            return a;
        }
        public void TextBox2ToNoteChanel(uint chanel)
            {
                 // запись в файл
                string buildnote = "note";
                string buildchanel = chanel.ToString();
                string buildend = ".txt";
                string buildsum = buildnote + buildchanel + buildend;
                using (FileStream fstream = new FileStream(@buildsum, FileMode.Create))//@"E:\note.txt"
                {
                    
                    string text = textBox2.Text;

                    // преобразуем строку в байты
                    byte[] array = System.Text.Encoding.Default.GetBytes(text);
                    // запись массива байтов в файл
                    fstream.Write(array, 0, array.Length);

                }

            }
        public long NoteChanelTolabel2(uint chanel)
        {
            long Price_for_Liter_local = 0;
            try
            {
                // чтение из файла
                string buildnote = "note";
                string buildchanel = chanel.ToString();
                string buildend = ".txt";
                string buildsum = buildnote + buildchanel + buildend;


                using (FileStream fstream = File.OpenRead(@buildsum))
                {
                    // преобразуем строку в байты
                    byte[] array = new byte[fstream.Length];
                    // считываем данные
                    fstream.Read(array, 0, array.Length);
                    // декодируем байты в строку
                    string textFromFile = System.Text.Encoding.Default.GetString(array);
                    //Console.WriteLine("Текст из файла: {0}", textFromFile);

                    //byte[] array1 = System.Text.Encoding.Default.GetBytes(textFromFile);
                    label2.Text = textFromFile;
                    Price_for_Liter_local = long.Parse(label2.Text);
                    return Price_for_Liter_local;
                }
            }
            catch
            {
                return Price_for_Liter_local;
            }

        }


       
        private void button5_Click(object sender, EventArgs e1)
        {

            string[] text = textBox1.Text.Split(' ');
            string a = text[0];
           
            label1.Text = a.ToString();
            
           
        }

        private void button8_Click(object sender, EventArgs e1)// Старт сделки
        {
            if(CurrentChanel == 1)
                nozzreq1.flag_start_transaction = 1;
            if (CurrentChanel == 2)
                nozzreq2.flag_start_transaction = 1;
            if (CurrentChanel == 3)
                nozzreq3.flag_start_transaction = 1;
            if (CurrentChanel == 4)
                nozzreq4.flag_start_transaction = 1;
            if (CurrentChanel == 5)
                nozzreq5.flag_start_transaction = 1;
            if (CurrentChanel == 6)
                nozzreq6.flag_start_transaction = 1;
            if (CurrentChanel == 7)
                nozzreq7.flag_start_transaction = 1;
            if (CurrentChanel == 8)
                nozzreq8.flag_start_transaction = 1;
            //button8.Enabled = false;
        }

        private void button10_Click(object sender, EventArgs e1)// Продолжить
        {
            if (CurrentChanel == 1)
                nozzreq1.flag_resume = 1;
            if (CurrentChanel == 2)
                nozzreq2.flag_resume = 1;
            if (CurrentChanel == 3)
                nozzreq3.flag_resume = 1;
            if (CurrentChanel == 4)
                nozzreq4.flag_resume = 1;
            if (CurrentChanel == 5)
                nozzreq5.flag_resume = 1;
            if (CurrentChanel == 6)
                nozzreq6.flag_resume = 1;
            if (CurrentChanel == 7)
                nozzreq7.flag_resume = 1;
            if (CurrentChanel == 8)
                nozzreq8.flag_resume = 1;
        }

        private void button9_Click(object sender, EventArgs e1)// Стоп
        {
            if (CurrentChanel == 1)
                nozzreq1.flag_stop = 1;
            if (CurrentChanel == 2)
                nozzreq2.flag_stop = 1;
            if (CurrentChanel == 3)
                nozzreq3.flag_stop = 1;
            if (CurrentChanel == 4)
                nozzreq4.flag_stop = 1;
            if (CurrentChanel == 5)
                nozzreq5.flag_stop = 1;
            if (CurrentChanel == 6)
                nozzreq6.flag_stop = 1;
            if (CurrentChanel == 7)
                nozzreq7.flag_stop = 1;
            if (CurrentChanel == 8)
                nozzreq8.flag_stop = 1;
        }

        private void sens_request(byte chanel)
        {
            //uint tmp_state = state1;
            
            if (chanel == 1)
            {
                nozzreq1.SwitchFunct();          
            }
            if (chanel == 2)
            {
                nozzreq2.SwitchFunct();          
            }
            if (chanel == 3)
            {
                nozzreq3.SwitchFunct();        
            }
            if (chanel == 4)
            {
                nozzreq4.SwitchFunct();           
            }
            if (chanel == 5)
            {
                nozzreq5.SwitchFunct();      
            }
            if (chanel == 6)
            {
                nozzreq6.SwitchFunct();       
            }
            if (chanel == 7)
            {
                nozzreq7.SwitchFunct();         
            }
            if (chanel == 8)
            {
                nozzreq8.SwitchFunct();    
            }

        }

 

                //label1.Text = result_release_out_out.ToString("f3");

                //label8.Text = u_adc.ToString("f0");



                //label9.Text = temperature.ToString("f");



                //StreamWriter sw;
               // sw = File.AppendText("logfile1.txt");
               // sw.WriteLine(label1.Text + "  " + label8.Text + "  " + label8.Text + "  " + DateTime.Now);
               // sw.Close();
        // }
        

        public void Write_Log(uint state)
        {
            lock (Form1.locker)
            {
                lock (Form1.locker)
                {
                    StreamWriter sw;
                    sw = File.AppendText("logfile1.txt");
                    sw.WriteLine(state.ToString() + "  " + DateTime.Now);
                    sw.Close();
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


     

      
        void LabelRefresh(NozzleRequest nozzreq, uint CurrentChanel)
        {
            label20.Text = nozzreq.lable20;
            //label22.Text = nozzreq.lable22;
            label23.Text = nozzreq.lable23;
            label25.Text = nozzreq.lable25;
            label26.Text = nozzreq.lable26;
            //label28.Text = nozzreq.lable28;
            label29.Text = nozzreq.lable29;
            label31.Text = nozzreq.lable31;
            label14.Text = nozzreq.lable14;
            label16.Text = nozzreq.lable16;
            label17.Text = nozzreq.lable17;
            //label19.Text = nozzreq.lable19;
            label3.Text = CurrentChanel.ToString();
        }
        private void button13_Click(object sender, EventArgs e)//кнопка 1
        {
            CurrentChanel = 1;
            nozzreq1.Price_for_Liter = NoteChanelTolabel2(CurrentChanel);
            LabelRefresh(nozzreq1, CurrentChanel);
            //pictureBox1.Image = Image.FromFile("pistol.jpg");
        }
        private void button14_Click(object sender, EventArgs e1)//кнопка 2
        {
            CurrentChanel = 2;
            nozzreq2.Price_for_Liter = NoteChanelTolabel2(CurrentChanel);
            LabelRefresh(nozzreq2, CurrentChanel);  
        }
       
        private void button16_Click(object sender, EventArgs e1)//кнопка 3
        {
            CurrentChanel = 3;
            nozzreq3.Price_for_Liter = NoteChanelTolabel2(CurrentChanel);
            LabelRefresh(nozzreq3, CurrentChanel);  
        }

        private void button15_Click(object sender, EventArgs e)//кнопка 4
        {
            CurrentChanel = 4;
            nozzreq4.Price_for_Liter = NoteChanelTolabel2(CurrentChanel);
            LabelRefresh(nozzreq4, CurrentChanel);  
        }

        private void button20_Click(object sender, EventArgs e)// кнопка 5
        {
            CurrentChanel = 5;
            nozzreq5.Price_for_Liter = NoteChanelTolabel2(CurrentChanel);
            LabelRefresh(nozzreq5, CurrentChanel);  
        }

        private void button19_Click(object sender, EventArgs e)// кнопка 6
        {
            CurrentChanel = 6;
            nozzreq6.Price_for_Liter = NoteChanelTolabel2(CurrentChanel);
            LabelRefresh(nozzreq6, CurrentChanel);  
        }

        private void button18_Click(object sender, EventArgs e)// кнопка 7
        {
            CurrentChanel = 7;
            nozzreq7.Price_for_Liter = NoteChanelTolabel2(CurrentChanel);
            LabelRefresh(nozzreq7, CurrentChanel);  
        }

        private void button17_Click(object sender, EventArgs e)// кнопка 8
        {
            CurrentChanel = 8;
            nozzreq8.Price_for_Liter = NoteChanelTolabel2(CurrentChanel);
            LabelRefresh(nozzreq8, CurrentChanel);  
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

      

      

     

        
        
      
    }

    public class NozzleRequest
    {
        public uint state;
        public uint nozzle;
        public char TransactionID;
        public long Price_for_Liter;
        private long Transaction_Money;
        private uint Transaction_Liters;
        public uint flag_start_transaction;
        public uint flag_resume;
        public uint flag_stop;
        public uint flag_end_was_sent;
        public byte chanel;
        public byte fuelling_auto;
        public byte stop_auto;
        public byte end_auto;

        public const uint ZeroState = 0;
        public const uint Idle = 1;
        public const uint Calling = 2;
        public const uint Authorised = 3;
        public const uint Started = 4;
        public const uint SuspendedStarted = 5;
        public const uint Fuelling = 6;
        public const uint SuspendedFuelling = 7;
        public const uint Stop = 8;
        public const uint End = 9;

        public string lable20;
        //public string lable22;
        public string lable23;
        public string lable25;
        public string lable26;
        //public string lable28;

        public string lable29;
        public string lable31;

        public string lable14;
        public string lable16;

        public string lable17;
        //public string lable19;

        public Form1 frm;

        public NozzleRequest(byte chanel_to_send)//конструктор
        {
          state = 0;
          nozzle = 0;
          TransactionID = '0'; 
          Price_for_Liter = 0;
          Transaction_Money = 0;
          Transaction_Liters = 0;
          flag_start_transaction = 0;
          flag_resume = 0;
          flag_stop = 0;
          flag_end_was_sent = 0;
          chanel = chanel_to_send;

          fuelling_auto = 1;
          stop_auto = 1;
          end_auto = 1;
        
        }

        public void RefreshDisplayChanel()
        {
           Price_for_Liter =  frm.TextBox2ToLabel2();
           if (Price_for_Liter > 0)
             frm.TextBox2ToNoteChanel(chanel);
        }
        //string a;
        //Form1 frm;
        //public void GW(string a, Form1 frm)
        //{
            //this.a = a;
           // this.frm = frm;
        //}

        public void SwitchFunct()
        {
            // this.frm = frm;
            switch (state)
            {
                case ZeroState://0

                    lock (Form1.locker)
                    {
                        // запись в файл
                        string buildnote = "logfile";
                        string buildchanel = chanel.ToString();
                        string buildend = ".txt";
                        string buildsum = buildnote + buildchanel + buildend;

                        StreamWriter sw;
                        sw = File.AppendText(buildsum);
                        sw.WriteLine("ZeroState спрашиваем статус" + "  " + DateTime.Now.Ticks);
                        sw.Close();
                    }

                   frm.Ask_Status(ref chanel, ref state, ref nozzle);

                   if (frm.CurrentChanel == chanel)
                    Price_for_Liter = frm.NoteChanelTolabel2(chanel);//chanel определяет файл из которого будет заливаться в текст лэйбл
                    //Price_for_Liter = long.Parse(label2.Text);

                    //frm.Write_Log(state);
                    break;
                case Idle://1
                    flag_end_was_sent = 0;
                    if (flag_start_transaction == 1)
                    {

                        lock (Form1.locker)
                        {

                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("Idle отправляем старт сделки" + "  " + DateTime.Now);
                            sw.Close();
                        }



                        flag_start_transaction = 0;
                        frm.Start_Transaction(ref chanel, ref state, ref nozzle, ref Transaction_Money, ref Transaction_Liters, ref Price_for_Liter);
                    }
                    else
                    {
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("Idle спрашиваем статус" + "  " + DateTime.Now);
                            sw.Close();
                        }
                        frm.Ask_Status(ref chanel, ref state, ref nozzle);
                    }
                    //Request_Transaction_Data();
                    //Request_Total_Counter();
                   // frm.Write_Log(state);
                    break;
                case Calling://2
                    if (flag_start_transaction == 1)
                    {
                        lock (Form1.locker)
                        {

                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("Calling отправляем старт сделки" + "  " + DateTime.Now);
                            sw.Close();
                        }

                        flag_start_transaction = 0;
                        frm.Start_Transaction(ref chanel, ref state, ref nozzle, ref Transaction_Money, ref Transaction_Liters, ref Price_for_Liter);
                    }
                    else
                    {
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("Calling спрашиваем статус" + "  " + DateTime.Now);
                            sw.Close();
                        }
                        frm.Ask_Status(ref chanel, ref state, ref nozzle);
                    }
                    //Request_Transaction_Data();
                    //Request_Total_Counter();
                    //frm.Write_Log(state);
                    break;
                case Authorised://3
                    if (flag_stop == 1)
                    {
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("Authorised отправляем стоп команду" + "  " + DateTime.Now);
                            sw.Close();
                        }

                        flag_stop = 0;
                        frm.StopCommand(ref chanel, ref state, ref nozzle);
                    }
                    else
                    {
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("Authorised спрашиваем статус" + "  " + DateTime.Now);
                            sw.Close();
                        }
                        frm.Ask_Status(ref chanel, ref state, ref nozzle);
                    }
                   // frm.Write_Log(state);
                    break;
                case Started://4
                    if (flag_stop == 1)
                    {
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("Started отправляем стоп команду" + "  " + DateTime.Now);
                            sw.Close();
                        }

                        flag_stop = 0;
                        frm.StopCommand(ref chanel, ref state, ref nozzle);
                    }
                    else
                    {
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("Started спрашиваем статус" + "  " + DateTime.Now);
                            sw.Close();
                        }
                        frm.Ask_Status(ref chanel, ref state, ref nozzle);
                    }
                   // frm.Write_Log(state);
                    break;
                case SuspendedStarted://5
                    if (flag_stop == 1)
                    {
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("SuspendedStarted отправляем стоп команду" + "  " + DateTime.Now);
                            sw.Close();
                        }

                        flag_stop = 0;
                        frm.StopCommand(ref chanel, ref state, ref nozzle);
                    }
                    else
                        if (flag_resume == 1)
                        {
                            lock (Form1.locker)
                            {
                                // запись в файл
                                string buildnote = "logfile";
                                string buildchanel = chanel.ToString();
                                string buildend = ".txt";
                                string buildsum = buildnote + buildchanel + buildend;

                                StreamWriter sw;
                                sw = File.AppendText(buildsum);
                                sw.WriteLine("SuspendedStarted отправляем возобновить команду" + "  " + DateTime.Now);
                                sw.Close();
                            }

                            flag_resume = 0;
                            frm.Resume(ref chanel, ref state, ref nozzle);
                        }
                        else
                            frm.Ask_Status(ref chanel, ref state, ref nozzle);
                    //frm.Write_Log(state);
                    break;
                case Fuelling://6
                    if (flag_stop == 1)
                    {
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("fueling отправляем стоп команду" + "  " + DateTime.Now);
                            sw.Close();
                        }

                        flag_stop = 0;
                        frm.StopCommand(ref chanel, ref state, ref nozzle);
                    }
                    else
                    {
                        
                        switch (fuelling_auto)
                        {
                            case 1:
                                lock (Form1.locker)
                                {
                                    // запись в файл
                                    string buildnote = "logfile";
                                    string buildchanel = chanel.ToString();
                                    string buildend = ".txt";
                                    string buildsum = buildnote + buildchanel + buildend;

                                    StreamWriter sw1;
                                    sw1 = File.AppendText(buildsum);
                                    sw1.WriteLine("fueling отправляем RT_V команду" + "  " + DateTime.Now);
                                    sw1.Close();
                                }

                                frm.Request_RT_V(ref chanel, ref state, ref nozzle, ref TransactionID, ref lable14, ref lable16);//L
                                fuelling_auto = 2;
                                break;
                            case 2:

                                lock (Form1.locker)
                                {
                                    // запись в файл
                                    string buildnote = "logfile";
                                    string buildchanel = chanel.ToString();
                                    string buildend = ".txt";
                                    string buildsum = buildnote + buildchanel + buildend;

                                    StreamWriter sw2;
                                    sw2 = File.AppendText(buildsum);
                                    sw2.WriteLine("fueling отправляем RT_М команду" + "  " + DateTime.Now);
                                    sw2.Close();
                                }

                                frm.Request_RT_M(ref chanel, ref state, ref nozzle, ref TransactionID, ref lable17/*, ref lable19*/);//R
                                fuelling_auto = 1;
                                break;
                            default:
                                fuelling_auto = 1;
                                break;
                        }

                        //frm.Ask_Status(ref chanel, ref state, ref nozzle);
                       // frm.Write_Log(state);
                    }
                    break;
                case SuspendedFuelling://7
                    if (flag_stop == 1)
                    {
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "logfile";
                            string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildchanel + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine("SuspendedFuelling отправляем стоп команду" + "  " + DateTime.Now);
                            sw.Close();
                        }


                        flag_stop = 0;
                        frm.StopCommand(ref chanel, ref state, ref nozzle);
                    }
                    else
                        if (flag_resume == 1)
                        {
                            lock (Form1.locker)
                            {
                                // запись в файл
                                string buildnote = "logfile";
                                string buildchanel = chanel.ToString();
                                string buildend = ".txt";
                                string buildsum = buildnote + buildchanel + buildend;

                                StreamWriter sw;
                                sw = File.AppendText(buildsum);
                                sw.WriteLine("SuspendedFuelling отправляем возобновить команду" + "  " + DateTime.Now);
                                sw.Close();
                            }

                            flag_resume = 0;
                            frm.Resume(ref chanel, ref state, ref nozzle);
                        }
                        else
                        {
                            lock (Form1.locker)
                            {
                                // запись в файл
                                string buildnote = "logfile";
                                string buildchanel = chanel.ToString();
                                string buildend = ".txt";
                                string buildsum = buildnote + buildchanel + buildend;

                                StreamWriter sw3;
                                sw3 = File.AppendText(buildsum);
                                sw3.WriteLine("SuspendedFuelling отправляем RT_V команду" + "  " + DateTime.Now);
                                sw3.Close();
                            }

                            frm.Request_RT_V(ref chanel, ref state, ref nozzle, ref TransactionID, ref lable14, ref lable16);
                            lock (Form1.locker)
                            {
                                // запись в файл
                                string buildnote = "logfile";
                                string buildchanel = chanel.ToString();
                                string buildend = ".txt";
                                string buildsum = buildnote + buildchanel + buildend;

                                StreamWriter sw4;
                                sw4 = File.AppendText(buildsum);
                                sw4.WriteLine("SuspendedFuelling отправляем RT_М команду" + "  " + DateTime.Now);
                                sw4.Close();
                            }

                            frm.Request_RT_M(ref chanel, ref state, ref nozzle, ref TransactionID, ref lable17/*, ref lable19*/);
                            //frm.Ask_Status(ref chanel, ref state, ref nozzle);
                           // frm.Write_Log(state);
                        }
                    break;
                case Stop://8
                    switch (stop_auto)
                    {
                        case 1:
                            lock (Form1.locker)
                            {
                                // запись в файл
                                string buildnote = "logfile";
                                string buildchanel = chanel.ToString();
                                string buildend = ".txt";
                                string buildsum = buildnote + buildchanel + buildend;

                                StreamWriter sw5;
                                sw5 = File.AppendText(buildsum);
                                sw5.WriteLine("Stop отправляем RT_V команду" + "  " + DateTime.Now);
                                sw5.Close();
                            }

                            frm.Request_RT_V(ref chanel, ref state, ref nozzle, ref TransactionID, ref lable14, ref lable16);
                            stop_auto = 2;
                            break;
                        case 2:

                            lock (Form1.locker)
                            {
                                // запись в файл
                                string buildnote = "logfile";
                                string buildchanel = chanel.ToString();
                                string buildend = ".txt";
                                string buildsum = buildnote + buildchanel + buildend;

                                StreamWriter sw6;
                                sw6 = File.AppendText(buildsum);
                                sw6.WriteLine("Stop отправляем RT_М команду" + "  " + DateTime.Now);
                                sw6.Close();
                            }

                            frm.Request_RT_M(ref chanel, ref state, ref nozzle, ref TransactionID, ref lable17/*, ref lable19*/);
                            stop_auto = 1;
                            break;
                        default:
                            end_auto = 1;
                            break;
                    }   
                    //Request_Transaction_Data();
                    //Request_Total_Counter();
                    //frm.Ask_Status(ref chanel, ref state, ref nozzle);  
                   // frm.Write_Log(state);
                    break;
                case End://9

                    if (flag_end_was_sent == 0)
                    {
                        switch (end_auto)
                        {
                            case 1:

                                lock (Form1.locker)
                                {
                                    // запись в файл
                                    string buildnote = "logfile";
                                    string buildchanel = chanel.ToString();
                                    string buildend = ".txt";
                                    string buildsum = buildnote + buildchanel + buildend;

                                    StreamWriter sw8;
                                    sw8 = File.AppendText(buildsum);
                                    sw8.WriteLine("End отправляем RT_V команду" + "  " + DateTime.Now);
                                    sw8.Close();
                                }

                                frm.Request_RT_V(ref chanel, ref state, ref nozzle, ref TransactionID, ref lable14, ref lable16);
                                end_auto = 2;
                             break;
                             case 2:


                                lock (Form1.locker)
                                {
                                    // запись в файл
                                    string buildnote = "logfile";
                                    string buildchanel = chanel.ToString();
                                    string buildend = ".txt";
                                    string buildsum = buildnote + buildchanel + buildend;

                                    StreamWriter sw9;
                                    sw9 = File.AppendText(buildsum);
                                    sw9.WriteLine("End отправляем RT_М команду" + "  " + DateTime.Now);
                                    sw9.Close();
                                }

                                frm.Request_RT_M(ref chanel, ref state, ref nozzle, ref TransactionID, ref lable17/*, ref lable19*/);
                                end_auto = 3;
                             break;
                             case 3:

                                lock (Form1.locker)
                                {
                                    // запись в файл
                                    string buildnote = "logfile";
                                    string buildchanel = chanel.ToString();
                                    string buildend = ".txt";
                                    string buildsum = buildnote + buildchanel + buildend;

                                    StreamWriter sw10;
                                    sw10 = File.AppendText(buildsum);
                                    sw10.WriteLine("End отправляем Request_Transaction_Data команду" + "  " + DateTime.Now);
                                    sw10.Close();
                                }

                                frm.Request_Transaction_Data(ref chanel, ref state,
                                ref lable20,
                                //ref lable22,
                                ref lable23,
                                ref lable25,
                                ref lable26,
                                //ref lable28,
                                ref nozzle, ref TransactionID);
                                
                                end_auto = 4;
                            break;
                            case 4:
                                lock (Form1.locker)
                                {
                                    // запись в файл
                                    string buildnote = "logfile";
                                    string buildchanel = chanel.ToString();
                                    string buildend = ".txt";
                                    string buildsum = buildnote + buildchanel + buildend;

                                    StreamWriter sw11;
                                    sw11 = File.AppendText(buildsum);
                                    sw11.WriteLine("End отправляем Request_Total_Counter команду" + "  " + DateTime.Now);
                                    sw11.Close();
                                }

                                frm.Request_Total_Counter(ref chanel, ref nozzle, ref lable29, ref lable31, ref  state);
                                end_auto = 5;
                            break;
                            case 5:
                                lock (Form1.locker)
                                {
                                    // запись в файл
                                    string buildnote = "logfile";
                                    string buildchanel = chanel.ToString();
                                    string buildend = ".txt";
                                    string buildsum = buildnote + buildchanel + buildend;

                                    StreamWriter sw7;
                                    sw7 = File.AppendText(buildsum);
                                    sw7.WriteLine("End отправляем End команду" + "  " + DateTime.Now);
                                    sw7.Close();
                                }

                                frm.End_Current_Transaction(ref chanel, ref state, ref nozzle, ref flag_end_was_sent);
                                end_auto = 1;
                            break;
                            default:
                            end_auto = 1;
                            break;

                        }
                    }
                    else
                    frm.Ask_Status(ref chanel, ref state, ref nozzle);

                   // frm.Write_Log(state);
                    break;
                default:
                    state = 0;
                    break;

            } 
        }

    }
    public class ClassLables
    {
        public string lable20;
        //public string lable22;
        public string lable23;
        public string lable25;
        public string lable26;
        //public string lable28;

        public Form1 frml;

       
    }
}
