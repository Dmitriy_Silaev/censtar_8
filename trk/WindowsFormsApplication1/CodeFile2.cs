﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO;



namespace WindowsFormsApplication1
{

    public partial class Form1 : Form
    {

        private void sens_request_4()
        {


            byte[] array = { 0x04, 0x47, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4B };    // запрос концентрации
            serialPort1.Write(array, 0, 8);

            Int32 byte0;
            Int32 byte1;
            Int32 byte2;
            Int32 byte3;
            Int32 byte4;
            Int32 byte5;
            Int32 byte6;
            Int32 byte7;
            Int32 ex_to = 0;

            Int32 result_release;
            short result_release_out;
            double result_release_out_out;
            double temperature;
            Int32 porog;
            try
            {
                byte0 = serialPort1.ReadByte();
                byte1 = serialPort1.ReadByte();
                byte2 = serialPort1.ReadByte();
                byte3 = serialPort1.ReadByte();
                byte4 = serialPort1.ReadByte();
                byte5 = serialPort1.ReadByte();
                byte6 = serialPort1.ReadByte();
                byte7 = serialPort1.ReadByte();
            }
            catch (TimeoutException e)
            {
                //return;
                //Console.WriteLine(e);
                byte0 = 0; byte1 = 0; byte2 = 0; byte3 = 0; byte4 = 0; byte5 = 0; byte6 = 0; byte7 = 0;
                ex_to = 1;
            }
            /*
            result_release = (double)(((byte6 & 0x80) << 9) | (byte2 << 8) | byte3) / 10;
            porog = byte6 & 0x03;
            temperature = (double)((byte4 << 8) | byte5) / 10;
            */
            if (ex_to == 0)
            {
                result_release = (byte2 << 8);
                result_release = (result_release + byte3);
                result_release_out = (short)result_release;
                result_release_out_out = (double)((double)result_release_out / 10);

                porog = byte6 & 0x03;
                temperature = (double)((byte4 << 8) | byte5) / 10;

                label17.Text = result_release_out_out.ToString("f");

                label18.Text = porog.ToString("f");



                label19.Text = temperature.ToString("f");



                StreamWriter sw;
                sw = File.AppendText("logfile4.txt");
                sw.WriteLine(label17.Text + "  " + label18.Text + "  " + label19.Text + "  " + DateTime.Now);
                sw.Close();
            }
        }
    }
}